I. Setup LLVM Environment - see the file "steps-i-took-for-4.0"
==================================

Steps:
---------------------------------
1. Create work dir
```
e.g.
mkdir /opt/scratch/gback/rpc/llvm
cd /opt/scratch/gback/rpc/llvm
```

2. Download LLVM-4.0.0 source code
```
wget http://releases.llvm.org/4.0.0/llvm-4.0.0.src.tar.xz
tar xf llvm-4.0.0.src.tar.xz 
```

3. Configure LLVM [1]  
change directory into the object root directory:  
`% cd OBJ_ROOT`  
run the cmake:  
`% cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=prefix=/install/path`
```
e.g. 
mkdir /opt/scratch/gback/rpc/llvm/llvm-install 
mkdir -p llvm-4.0.0.src/build
cd llvm-4.0.0.src/build/
cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=/opt/scratch/gback/rpc/llvm/llvm-install /opt/scratch/gback/rpc/llvm/llvm-4.0.0.src
```

4. build & install
```
time make -j 30
time make install
```

5. Reference  
[1] LLVM online doc: http://llvm.org/docs/GettingStarted.html#local-llvm-configuration

  
    
    
II.Build LLVM Pass
================================

Steps:
---------------------------------

1. Change path in the build.sh  
`BUILDDIR = llvm/build/path`
```
e.g.
BUILDDIR= ~/mytools/LLVM-4.0.0/llvm-4.0.0.src/build
```

2. Run build.sh
```
bash build.sh
```

3. Build the Pass
```
cd build
make
```

4. Test
```
cd test
bash runit3.sh
```
Tips:   
The version of opt must be the same version of the LLVM version we are building the pass.  
Can use the opt in the llvm-install path we built before. e.g. change the runit.sh
```
/opt/scratch/gback/rpc/llvm/llvm-install/bin/opt -load ../build/callgraph/libLLVMCGraphDisplay.so -cgraph < callgraphtestO0.bc >/dev/null
```
5. Reference  
http://llvm.org/docs/CMake.html#cmake-out-of-source-pass  
https://stackoverflow.com/questions/39665142/facing-issue-with-makefile-for-hello-pass-in-llvm

