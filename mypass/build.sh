

# Follows:
# http://llvm.org/docs/CMake.html#cmake-out-of-source-pass
# https://stackoverflow.com/questions/39665142/facing-issue-with-makefile-for-hello-pass-in-llvm

BUILDDIR=/home/kibur1/rpc/llvm/llvm-4.0.0.src/build
rm -rf build
mkdir build
(cd build; cmake -DCMAKE_PREFIX_PATH=${BUILDDIR} ..)
make -f build/rpc_transform/Makefile
make -f build/partition_func/Makefile
make -f build/static_analysis/Makefile
