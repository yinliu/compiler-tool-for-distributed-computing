#ifndef FunctionAnalysis_HPP__
#define FunctionAnalysis_HPP__

#include "llvm/IR/Function.h"
#include <llvm/Support/raw_ostream.h>
#include <stdio.h>

using namespace std;
using namespace llvm;


class FunctionAnalysis {
private:
	Function *mFn;

public:
	FunctionAnalysis(Function *fD) {
		mFn = fD;
	}

	int getReturnType(std::string &returnType);
	
	int getArgInfo(int &argNum, std::vector<std::string> &argName, std::vector<std::string> &argType);

	int getSize(std::vector<int> &size);
	int isSynchronize();
	int isSharedBuffer();

	std::string toNormalType(std::string IRType);
	std::string toNormalGlobalVarType(std::string IRType);

};


int FunctionAnalysis::getSize(vector<int>& size) {
	if (!mFn)
	  return 0;
	//FIXME here need to check the annotation in llvm IR level

// 	int argNum = mFn->getNumParams();
// 	// get function arguments infos (name, type)
// 	for (int i = 0; i < argNum; i++) {
// 		ParmVarDecl* p = mFn->parameters()[i];
// 
// 		if (!(p->hasAttrs())) {
// 			size.push_back(-1);
// 			continue;
// 		}
// 
// 		for (auto it : p->attrs()) {
// 			Attr& attr = (*it);
// 			const char * attrSpelling = attr.getSpelling();
// 			if (std::strcmp(attrSpelling, "paramlen") == 0) {
// 				auto paramLen = dyn_cast < ParamLenAttr > (&attr);
// 				int annotationValue = paramLen->getParamlen();
// 				llvm::outs() << "annotationValue ==" << annotationValue << "\n";
// 				size.push_back(annotationValue);
// 			}
// 		}
// 	}
	return 1;
}


int FunctionAnalysis::getReturnType(string& returnType) {
	if (!mFn)
	  return 0;
	// get function return type
	std::string str_FnReturnType;
        llvm::raw_string_ostream os_FnReturnType(str_FnReturnType);
	mFn->getReturnType()->print(os_FnReturnType);
	
	
	std::string temp_type = os_FnReturnType.str();
	returnType = toNormalType(temp_type);
	
	return 1;
}

int FunctionAnalysis::getArgInfo(int& argNum, vector< string >& argName, vector< string >& argType)
{
        if (!mFn)
	  return 0;
	for (Argument & args : mFn->getArgumentList()) {
	      std::string temp_name = args.getName().str();
	      argName.push_back(temp_name);
      
	      std::string str_ArgType;
	      llvm::raw_string_ostream os_ArgType(str_ArgType);
	      args.getType()->print(os_ArgType);
      
	      std::string temp_type = os_ArgType.str();

	      errs()<< "!!!!!!!!!!!! type =======" << temp_type <<"\n";
      
	      //NOTE the array parameters in the IR level is the pointer like "xxx *"
	      std::string type;
      
	      type = toNormalType(temp_type);

	      argType.push_back(type);

	      argNum++;      
            }
}

string FunctionAnalysis::toNormalType(string IRType)
{
	      std::string type;
	      if(IRType == "i32"){
		type = "int";
	      } else if (IRType == "i32*"){
		type = "int*";
	      } else if (IRType == "i16"){
		type = "short";
	      } else if (IRType == "i16*"){
		type = "short*";
	      } else if (IRType == "i64"){
		type = "long";
	      } else if (IRType == "i64*"){
		type = "long*";
	      } else if (IRType == "i8"){
		type = "char";
	      } else if (IRType == "i8*"){
		type = "char*";
	      } else {
		type = IRType;
	      }

return type;
}


string FunctionAnalysis::toNormalGlobalVarType(string IRType)
{
  std::string type;
  type = IRType.substr(0, (IRType.size()-1));
  return toNormalType(type);
}




#endif
