#define DEBUG_TYPE "mycgraph"
#include "llvm/Analysis/CallGraph.h"
#include "llvm/Analysis/CallGraphSCCPass.h"
#include "llvm/IR/Module.h"
#include "llvm/Pass.h"
#include "llvm/Support/raw_ostream.h"

#include "../util/Demangling.hpp"
#include "../util/Annotation.hpp"
#include "../util/common.h"

#define DEBUG 0

using namespace llvm;

namespace {
  struct MyCGraph : public ModulePass {
    static char ID; // Pass identification, replacement for typeid
    MyCGraph() : ModulePass(ID) {}
    MyCGraph(std::set<Function*> partition_fns) : ModulePass(ID) {
      secure_fns = partition_fns;
    }
    //MyCGraph() : ModulePass(ID){initializeMyCGraph Pass (*PassRegistry::getPassRegistry());}
    
    //FIXME for the multiple annotated function, need to deal with:
    // 1. if the related function set of annotated function A is totally different (no any dependencies) with that of annotated function B
    //    then, if the set of annotated function A could be partitioned, but function B cannot be partitioned, 
    //          then, we can only partition function A and its related function. meanwhile, warn the user that function B cannot be partitioned
    // 2. if these two sets (or more) have dependencies, then, they are partitioned or unpartitioned together
    // should we consider this sort of "complex" situation?
    
  public:
    std::set<Function*> secure_fns;
    int isPartition = -1;
    // store the global variables' name in this set
    std::set<std::string> gvNameSet;
    std::set<std::string> gvTypeSet;
    std::set<struct GlobalVarDecl> gvSet;
    std::set<struct GlobalVarDecl> gvCanPartitionedSet;
    
    // store the global variables' name which is used by the related functions
    //FIXME should not use like this, should use pair<name, type>
    std::set<std::string> gvNameUsedSet;
    std::set<std::string> gvTypeUsedSet;
    std::set<struct FunctionInfo> fnInfoSet;
    
    // FIXME not used by the related functions, is used be the non-related functions!!!
     std::set<std::string> unrelatedFnNameSet;

    // store the related functions' name in this set
    std::set<std::string> relatedFnNameSet;
    
    std::set<std::string> canBePartitionedFnNameSet;
    std::set<std::string> noZigzagFnNameSet;
    std::set<std::string> zigzagFnNameSet;

    std::set<std::string> relatedFnGVSet;
    std::set<std::string> otherFnGVSet;

    std::vector<CallGraphNode*> order;
    std::map<CallGraphNode*, int> vis;

    void generateCallGraph(CallGraph &callgraph, Function* fn);
    
    void checkZigZag(CallGraph &callgraph, std::set<Function*> annotated_fns);
    void checkGlobalVars(llvm::Module &m);
    void constructFnInfo(llvm::Module &m, std::set<Function*> annotated_fns);
    void checkPartition(CallGraph &callgraph, std::set<Function*> annotated_fns);

    void getGlobalVars(llvm::Module &m);
    void getUsedGlobalVars(Function& fn);
    int partitionCheck(llvm::Module &m);

    void analyzeGlobalVar(llvm::Module &m);
    
    void analyzeGlobalVar(Function& fn);
    
    void dfsTraverse(CallGraphNode* cgNode);
    
    std::string toNormalType(std::string IRType);
    std::string toNormalGlobalVarType(std::string IRType);

    virtual bool runOnModule(llvm::Module &m) {
	//std::set<Function*> secure_fns = getAnnotatedFn(m);
	//dumpAnnotatedObj(m); // for annotate the object

	//NOTE we can get the function simply with its name
	//m.getFunction("functionName");

	CallGraph testCG(m);
	testCG.dump();

	//1. check global variables
	checkGlobalVars(m);
	
	//2. check zigzag function
	//put zigzag function to std::set zigzagFnNameSet
	//put non-zigzag function to std::set noZigzagFnNameSet
	checkZigZag(testCG, secure_fns);
	
	
	// 3. construct the function info based on result in 1,2
	constructFnInfo(m, secure_fns);
	
	// 4. check whether can be partition for each annotated function
	checkPartition(testCG, secure_fns);
	//if(DEBUG){
	  for(struct FunctionInfo fn : fnInfoSet){
	    errs()<<"function name: "<< fn.Name<<"\n";
	    
	    if(fn.hasGlobalVars)
	      errs()<<"hasGlobalVars"<<"\n";
	    else
	      errs()<<"NOT hasGlobalVars"<<"\n";
	    
	    for(auto var : fn.globalVarDecls){
	      errs()<<"used global var: "<< var.Name <<"\n";
	    }
	    
	    if(fn.canBePartition)
	      errs()<<"can be partitioned"<<"\n";
	    else
	      errs()<<"can NOT be partitioned"<<"\n";
	     errs()<<"reason: "<< fn.reportInfo<<"\n";
	  }
	//}
	// 5. check results

// 	//3. check whether the noZigZag function contain global variable
// 	for (auto fnName : noZigzagFnNameSet){
// 	  Function* fn = m.getFunction(fnName);
// 	  getUsedGlobalVars(*fn);
// 	}
// 	isPartition = partitionCheck(m);
	
	
	//4. construct annotated function info for reporting
	
	//3.3 if they are contain, else ...


        return true;

	//1. get Global variables
	//getGlobalVars(m);

	//2. get call graph
	//use DFS to traverse it first? or simply traverse by calling llvm API?
	//get all related functions that should be treated as secure, and should be partitioned
	//after that relatedFnNameSet will be filled
// 	for(auto fn : secure_fns){
// 	    generateCallGraph(testCG, fn);
// 	}
	
// 	//2.1 get the non-related function set --- maybe don't need, just keep it here
//         for(auto &f : m) {
// 	  std::string name = f.getName();
// 	  int sign = 0;
// 	  for (auto relatedFnName : relatedFnNameSet){
// 	    if(name == relatedFnName){
// 	      sign = 1;
// 	      break;
// 	    }
// 	  }
// 	  if(sign == 0){
// 	    if(DEBUG)
// 	      errs()<< "unrelatedFnNameSet = " << name << "\n";
// 	    unrelatedFnNameSet.insert(name);
// 	  }
// 	}

	//3. check whether it contains global vars
	//analyzeGlobalVar(m);

	//3.1 analyze which global variables the related function will be called
// 	for (auto &fn : m) {
// 	  analyzeGlobalVar(fn);
// 	}
        //get the global vars using by all non-related functions

// 	for (auto fnName : relatedFnNameSet){
// 	  Function* fn = m.getFunction(fnName);
// 	  getUsedGlobalVars(*fn);
// 	}

	//3.2 analyze which global variables will be called by other functions
// 	isPartition = partitionCheck(m);

// 	for (auto &fn : m) {
// 	   std::string fnName = fn.getName();
// 	   if(fnName.empty() ||  relatedFnNameSet.find(fnName) == relatedFnNameSet.end()){
// 	     int isPartition = partitionCheck(fn);
// 	     if(isPartition){
// 	       
// 	     } else {
// 	      
// 	     }
// 	  }
// 	}

    }

    virtual bool doInitialization (CallGraph &CG) {
      errs() << "Call Graph Display initialized... " << '\n';
      return false;
    }

    // We don't modify the program, so we preserve all analyses.
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
      //should we add this require?
      //AU.addRequired<CallGraph>();
      ///////////////////////////////
      AU.setPreservesAll();
    }
    
    Pass* createMyCGraphPass() { return new MyCGraph(); }
    
    
  };
}

int MyCGraph::partitionCheck(llvm::Module &m)
{
  // if un-annotated function uses the global, then, it cannot be partitioned
  for (auto &fn : m) {
    std::string fnName = fn.getName();
    if(fnName.empty() || relatedFnNameSet.find(fnName) != relatedFnNameSet.end()){
      continue;
    }
        for (auto &basic_block : fn) {
            for (auto &inst : basic_block) {
	        llvm::Instruction* testInst = &inst;
		if(testInst == NULL && testInst->getFunction() == NULL){
		  continue;
		}
		std::string testStr;
		raw_string_ostream OS(testStr);
		testInst->print(OS);
		StringRef compareStr(OS.str());

		for(auto name : gvNameUsedSet){
		    // here we can check the given name, and see whether it contains in the instruction
		    // in short, if the function contains the global variable, then we wouldn't partition it
		    string nameCondition1 = "@";
		    string nameCondition2 = "%";
		    nameCondition1.append(name);
		    nameCondition2.append(name);
		    if(DEBUG){
		      errs()<< "nameCondition1 == " << nameCondition1;
		      errs()<< "nameCondition2 == " << nameCondition2;
		      errs()<< "compareStr == " << compareStr;
		    }
		    if(compareStr.contains(nameCondition1) || compareStr.contains(nameCondition2)){
			return 0;
		    }
		}
            }
        }
  }
  return 1;
}

void MyCGraph::getGlobalVars(Module& m)
{
   errs() << "getGlobalVars ++" << "\n";    
	// here we can get all global variables
	for (llvm::Module::global_iterator ii = m.global_begin(); 
                          ii != m.global_end(); ++ii) {
	          struct GlobalVarDecl gvDecl = {};
                  GlobalVariable* gv = &(*ii);
		  StringRef gvName (gv->getName());
	          std::string str_Type;
		  llvm::raw_string_ostream os_Type(str_Type);
		  //WARNING global variables are always pointers, e.g., for gloable variable - int a; here will get the type "i32*"
		  //so that we need to remove one '*'
		  gv->getType()->print(os_Type);
		  // if the global variable's name contains "."
		  // then it might be the ".str.x" or "llvm.global.annotations"
		  if (!gvName.contains(".")){
		    if(DEBUG)
		      errs() << "gv's name == " << gvName << "\n";
		    
		    //FIXME some global variable is unnecessary to check, e.g., stderr 
		    if(gvName == "stderr") continue;
                    //////////////////////////////////////////////////////////////////
		    
		    gvNameSet.insert(gvName);
		    gvDecl.Name = gvName.str();
		    if(DEBUG)
		      errs() << "gv's type == " << os_Type.str() << "\n";    
		    std::string type = toNormalGlobalVarType(os_Type.str());
		    //gvTypeSet.insert(os_Type.str());
		    gvTypeSet.insert(type);
		    gvDecl.Type = type;

		      for (llvm::Value::use_iterator jj = gv->use_begin(); 
			    jj != gv->use_end(); ++jj) { 
			  // jj is a reference for the user of the global variable
			  auto fnName = &(*jj);
		          
			  fnName->get()->dump();
		      
		          auto user = fnName->getUser();
			  llvm::Instruction* testin = (llvm::Instruction*)user;
		      
		      user->dump();
		      	  //if(DEBUG)
			    errs()<< "user's name" << testin->getFunction()->getName()<< "\n";
			    errs()<< "fnName's name" << fnName->get()->getName()  << "\n";
			  //auto fnName = jj;
		      } 
		  }
        }
        errs() << "getGlobalVars --" << "\n";  

}



void MyCGraph::checkGlobalVars(Module& m)
{
   errs() << "checkGlobalVars ++" << "\n";    
	// here we can get all global variables
	for (llvm::Module::global_iterator ii = m.global_begin(); 
                          ii != m.global_end(); ++ii) {
	          struct GlobalVarDecl gvDecl = {};
                  GlobalVariable* gv = &(*ii);
		  StringRef gvName (gv->getName());
	          std::string str_Type;
		  llvm::raw_string_ostream os_Type(str_Type);

		  gv->getType()->print(os_Type);
		  // NOTE if the global variable's name contains "."
		  // then it might be the ".str.x" or "llvm.global.annotations"
		  if (!gvName.contains(".")){
		    //NOTE some global variable is unnecessary to check, e.g., stderr 
		    if(gvName == "stderr" || gvName == "stdout" || gvName == "stdin") continue;
                    //////////////////////////////////////////////////////////////////

		    if(DEBUG)
		      errs() << "gv's name == " << gvName << "\n";		    
		    gvDecl.Name = gvName.str();

		    if(DEBUG)
		      errs() << "gv's type == " << os_Type.str() << "\n";    
		    gvDecl.Type = toNormalGlobalVarType(os_Type.str());
		      errs() << "gvDecl.Type == " << gvDecl.Type << "\n";
		    // check which function does use this global variable
		      for (llvm::Value::use_iterator jj = gv->use_begin(); 
			    jj != gv->use_end(); ++jj) {
			  // jj is a reference for the user of the global variable
			  auto use = &(*jj);
		          User* user = use->getUser();
		          if (user != NULL && isa<Instruction>(user)) {
			    llvm::Instruction* test = (llvm::Instruction*)(user);
			    //llvm::Instruction* test = dyn_cast<llvm::Instruction*>(user);
			    auto userFn = test->getFunction();
			      //auto userFn = static_cast<llvm::Instruction*>(user)->getFunction();
			      if (userFn != NULL) {
				gvDecl.userFnName.insert(userFn->getName());
				if(DEBUG){
				  use->get()->dump();
				  errs()<< "user's name" << ((llvm::Instruction*)user)->getFunction()->getName()<< "\n";
				  errs()<< "fnName's name" << use->get()->getName()  << "\n";
				  user->dump();
				}
			      }
			  }
		      }
		      gvSet.insert(gvDecl);
		  }
        }
        errs() << "checkGlobalVars --" << "\n";
}

void MyCGraph::constructFnInfo(llvm::Module &m, std::set<Function*> annotated_fns)
{
  for (auto fn : annotated_fns){
    if(fn == NULL) continue;
    struct FunctionInfo fnInfo = {};
    fnInfo.Name = fn->getName();
    if(zigzagFnNameSet.find(fnInfo.Name) != zigzagFnNameSet.end()){
      errs()<<"set " <<  fnInfo.Name<< " zigzag == true"<<"\n";
      fnInfo.isZigZag = true;
    } else {
      errs()<<"set " <<  fnInfo.Name<< " zigzag == false"<<"\n";
      fnInfo.isZigZag = false;
    }

    for (auto var : gvSet){
      if (var.userFnName.find(fnInfo.Name) != var.userFnName.end()){
	//this function uses that global var
	fnInfo.hasGlobalVars = true;
	fnInfo.globalVarDecls.insert(var);
      }     
    }
    fnInfoSet.insert(fnInfo);
  }
}

void MyCGraph::checkPartition(CallGraph &callgraph, std::set<Function*> annotated_fns)
{
  for(struct FunctionInfo mfnInfo: fnInfoSet){
    errs()<<"checkPartition " <<  mfnInfo.Name<<"\n";
    struct FunctionInfo fnInfo = mfnInfo;
    if(fnInfo.isZigZag){
      // check zigzag
      errs()<<"set " <<  fnInfo.Name<< " canBePartition == false"<<"\n";
      fnInfo.canBePartition = false;
      fnInfo.reportInfo = "Cannot be partitioned Because it is zigzag function";
    } else {
      // check global variable and group(在同一个闭包里)
      if(fnInfo.hasGlobalVars ==  false) {
	fnInfo.canBePartition = true;
	continue;
      }

      //if and only if a global variable is used by noZigZag set, then, it can be partitioned
       for (auto var : fnInfo.globalVarDecls){
	 //NOTE std::set_difference
	 //refer to http://en.cppreference.com/w/cpp/algorithm/set_difference
         std::set<string> diff;
         std::set_difference(var.userFnName.begin(), var.userFnName.end(),
			     noZigzagFnNameSet.begin(), noZigzagFnNameSet.end(), 
                             std::inserter(diff, diff.begin()));
	 if(diff.size() != 0){
	   // it means that some elements of var.userFnName are not in the set noZigzagFnNameSet
	   // Thus, it cannot be partitioned
	   fnInfo.canBePartition = false;
	   fnInfo.reportInfo = "Cannot be partitioned Because it has global vars used by other functions that cannot be partitioned";
	 }
       }
    }
    fnInfoSet.erase(mfnInfo);//may lead to error
    fnInfoSet.insert(fnInfo);
    
  }

  
  std::set<std::string> resetFnName;

    // check group (同一个闭包里，一个不行就全不行)
  for(struct FunctionInfo mfnInfo: fnInfoSet){
     struct FunctionInfo fnInfo = mfnInfo;

     if(fnInfo.canBePartition)
	errs()<<"function can be partition" <<  fnInfo.Name<< " is "<< "true" <<"\n";
     else
       errs()<<"function can be partition" <<  fnInfo.Name<< " is "<< "false" <<"\n";
    if(fnInfo.isZigZag == true && fnInfo.canBePartition == false) {
      errs()<<"continure?"<<"\n";
      continue;
    }
    
    for(auto fn : annotated_fns){
      if(fn == NULL || fn->getName() != fnInfo.Name)
	continue;

      errs()<<" fn->getName() = "<< fn->getName() <<"\n";
      CallGraphNode* cgNode =  callgraph.operator[](fn);
      vis.clear();
      order.clear();
      dfsTraverse(cgNode);

      // "order" set contains all root function's callees
      int sign = 0;
      int needReset = 0;
//       std::set<struct FunctionInfo> remove;
//       std::set<struct FunctionInfo> add;
      for(auto callee : order){
	if(callee == NULL || callee->getFunction() == NULL){
	  continue;
	}
	string calleeName = callee->getFunction()->getName();
	errs()<<" calleeName =" << calleeName <<"\n";

	for(struct FunctionInfo calleeFn: fnInfoSet){
	  //struct FunctionInfo calleeFn = mCalleeFn;
	  errs()<<" calleeFn.Name =" << calleeFn.Name <<"\n";
	  if(calleeFn.Name == calleeName){
	    if(calleeFn.canBePartition == false){
	      needReset = 1;
	    }
	  }
	}
      }
      
      if (needReset == 1){
	for(auto callee : order){
	  if(callee == NULL || callee->getFunction() == NULL){
	    continue;
	  }
	  string calleeName = callee->getFunction()->getName();
	  errs()<<" calleeName =" << calleeName <<"\n";
	  for(struct FunctionInfo mCalleeFn: fnInfoSet){
	    struct FunctionInfo calleeFn = mCalleeFn;
	    if(calleeFn.Name == calleeName){
	      if(calleeFn.canBePartition == true){
		errs()<<" function fnInfo =" << calleeFn.Name << " will be set canBePartition false " <<"\n";
		resetFnName.insert(calleeFn.Name);
	      }
	      
	    }
	  }
	}
      }
   
    
    }
    vis.clear();
    order.clear();
  }
  
  
  for(struct FunctionInfo mfnInfo: fnInfoSet){
     struct FunctionInfo fnInfo = mfnInfo;
     for(auto fnName : resetFnName){
       if(fnName == fnInfo.Name){
	 fnInfo.canBePartition = false;
	 fnInfo.reportInfo = "Cannot be partitioned Because in its group has some functions cannot be partitioned";
	 fnInfoSet.erase(mfnInfo);//may lead to error
         fnInfoSet.insert(fnInfo);
      }
    }
		
  }
  
  /*
   * below is obsoleted solution
   */
  /*
  // check group (同一个闭包里，一个不行就全不行)
  for(struct FunctionInfo mfnInfo: fnInfoSet){
     struct FunctionInfo fnInfo = mfnInfo;

     if(fnInfo.canBePartition)
	errs()<<"function can be partition" <<  fnInfo.Name<< " is "<< "true" <<"\n";
     else
       errs()<<"function can be partition" <<  fnInfo.Name<< " is "<< "false" <<"\n";
    if(fnInfo.isZigZag == true && fnInfo.canBePartition == false) {
      errs()<<"continure?"<<"\n";
      continue;
    }
    
    for(auto fn : annotated_fns){
      if(fn == NULL || fn->getName() != fnInfo.Name)
	continue;

      errs()<<" fn->getName() = "<< fn->getName() <<"\n";
      CallGraphNode* cgNode =  callgraph.operator[](fn);
      vis.clear();
      order.clear();
      dfsTraverse(cgNode);

      // "order" set contains all root function's callees
      int sign = 0;
      int needReset = 0;
//       std::set<struct FunctionInfo> remove;
//       std::set<struct FunctionInfo> add;
      for(auto callee : order){
	if(callee == NULL || callee->getFunction() == NULL){
	  continue;
	}
	string calleeName = callee->getFunction()->getName();
	errs()<<" calleeName =" << calleeName <<"\n";

	for(struct FunctionInfo calleeFn: fnInfoSet){
	  //struct FunctionInfo calleeFn = mCalleeFn;
	  errs()<<" calleeFn.Name =" << calleeFn.Name <<"\n";
	  if(calleeFn.Name == calleeName){
	    if(calleeFn.canBePartition == false){
	      needReset = 1;
	    }
	  }
	}
      }
      
      if (needReset == 1){
	for(auto callee : order){
	  if(callee == NULL || callee->getFunction() == NULL){
	    continue;
	  }
	  string calleeName = callee->getFunction()->getName();
	  errs()<<" calleeName =" << calleeName <<"\n";
	  for(struct FunctionInfo mCalleeFn: fnInfoSet){
	    struct FunctionInfo calleeFn = mCalleeFn;
	    if(calleeFn.Name == calleeName){
	      if(calleeFn.canBePartition == true){
		errs()<<" function fnInfo =" << calleeFn.Name << " will be set canBePartition false " <<"\n";
		calleeFn.canBePartition = false;
		calleeFn.reportInfo = "Cannot be partitioned Because in its group has some functions cannot be partitioned";
	      }
	      
	    }
	    fnInfoSet.erase(mCalleeFn);
	    fnInfoSet.insert(calleeFn);
	  }
	}
      }
   
    
    }
    vis.clear();
    order.clear();
  } 
  */


}
  





// get global vars used by related functions
void MyCGraph::getUsedGlobalVars(Function& fn)
{
        for (auto &basic_block : fn) {
            for (auto &inst : basic_block) {
		/* NOTE here also can get the opcode name by using getOpcodeName to check whether the global variable is changed
		 * refer to LLVM cookbook - "Writing an analysis pass"
		 * http://proquest.safaribooksonline.com.ezproxy.lib.vt.edu/book/programming/9781785285981/4dot-preparing-optimizations/ch04s03_html#X2ludGVybmFsX0h0bWxWaWV3P3htbGlkPTk3ODE3ODUyODU5ODElMkZjaDA0czA3X2h0bWwmcXVlcnk9
                 */
		/* NOTE how to print the instruction -- AsmWriter.cpp
		 * void AssemblyWriter::printInstruction(const Instruction &I)
		 */
		llvm::Instruction* testInst = &inst;
		std::string testStr;
		raw_string_ostream OS(testStr);
		testInst->print(OS);
		StringRef compareStr(OS.str());

		//FIXME here should not be the "gvNameSet", instead, is the related function's global set
		if(DEBUG)
		  errs()<< "compareStr === " << compareStr.str() << "\n";
		auto it_name = gvNameSet.begin();
		auto it_type = gvTypeSet.begin();
		while(it_name != gvNameSet.end()){
		  std::string name = *it_name;
		  std::string nameCon1 = "@";
		  nameCon1.append(name);
		  std::string nameCon2 = "%";
		  nameCon2.append(name);
		  
		  if(compareStr.contains(nameCon1) || compareStr.contains(nameCon2)){
		    if(DEBUG)
		      errs()<< "caonima name === " << name << "\n";
		    if(testInst != NULL && testInst->getFunction() != NULL){
			if(DEBUG) {  
			  errs()<< "can I use this tricy way?" << OS.str() << "\n";
			  errs()<< "function's name ==== " << testInst->getFunction()->getName() <<"\n";
			}
			  // store which global variable has been used by related function
			  gvNameUsedSet.insert(name);
			  std::string type = *it_type;
			  gvTypeUsedSet.insert(type);
		      
		    }
		  }
		  ++it_name;
		  //FIXME might cause error here, need to change two sets to one pair<>
		  ++it_type;
		}
		
		/*
		while(it_name != gvNameSet.end() && it_type != gvTypeSet.end()){
		    // here we can check the given name, and see whether it contains in the instruction
		    // in short, if the function contains the global variable, then we wouldn't partition it
		    std::string name = *it_name;
		    errs()<< "caonima name === " << name << "\n";
		    //FIXME the case about %+name and @+name
		    if(compareStr.contains(name)){
			//if(the function name is not in the secure function set)
			if(testInst != NULL && testInst->getFunction() != NULL){
			  errs()<< "can I use this tricy way?" << OS.str() << "\n";
			  errs()<< "function's name ==== " << testInst->getFunction()->getName() <<"\n";
			  //relatedFnGVSet.insert(testInst->getFunction()->getName());
			  
			  // store which global variable has been used by related function
			  gvNameUsedSet.insert(name);
			  std::string type = *it_type;
			  gvTypeUsedSet.insert(type);
			}

		    }
		    ++it_name;
		    ++it_type;
		}
		*/
            }
        }
}



void MyCGraph::analyzeGlobalVar(Function& fn)
{
  // will find the global variable and update this set during traverse
  std::set<std::string>globalVarName;

        for (auto &basic_block : fn) {
            for (auto &inst : basic_block) {
	        llvm::Instruction* testInst = &inst;
	        //errs()<< "function's name ==== " << testInst->getFunction()->getName() <<"\n";
		//how to print the instruction -- AsmWriter.cpp
		//void AssemblyWriter::printInstruction(const Instruction &I)
		std::string testStr;
		raw_string_ostream OS(testStr);
		testInst->print(OS);
		StringRef compareStr(OS.str());

		//FIXME here should not be the "gvNameSet", instead, is the related function's global set
		for(auto name : gvNameSet){
		    // here we can check the given name, and see whether it contains in the instruction
		    // in short, if the function contains the global variable, then we wouldn't partition it
		    if(compareStr.contains(name)){
			//if(the function name is not in the secure function set)
			if(testInst != NULL && testInst->getFunction() != NULL){
			  if(DEBUG) {
			    errs()<< "can I use this tricy way?" << OS.str() << "\n";
			    errs()<< "function's name ==== " << testInst->getFunction()->getName() <<"\n";
			  }
			  //relatedFnGVSet.insert(testInst->getFunction()->getName());
			}

		    }
		}
            }
        }
}



/*
 * generate all related functions via callgraph
 * how to deal with the problem that there are two different call graphs that connect together
 * fn should be the secure function
 * here we can use the DFS to check each node
 */

/**********************  图的深度优先搜寻法********************/
// void dfs(int current)
// {
//    graph ptr;
//    visited[current] = 1;          /* 记录已遍历过       */
//    printf("vertex[%d]\n",current);   /* 输出遍历顶点值     */
//    ptr = head[current].nextnode;  /* 顶点位置           */
//    while ( ptr != NULL )          /* 遍历至链表尾       */
//    {
//       if ( visited[ptr->vertex] == 0 )  /* 如过没遍历过 */
//          dfs(ptr->vertex);              /* 递回遍历呼叫 */
//       ptr = ptr->nextnode;              /* 下一个顶点   */
//    }
// }

void MyCGraph::dfsTraverse(CallGraphNode* cgNode)
{
 if(DEBUG)
  errs() << "push_back :" << cgNode->getFunction()->getName() << "\n";
 order.push_back(cgNode);
 vis[cgNode] = 1;
 
 for (int i = 0; i < cgNode->size(); i++)
 {
    CallGraphNode * subNode = cgNode->operator[](i);
    if (subNode != NULL && subNode->getFunction() != NULL){
      if(DEBUG){
	errs() << "before do dfsTraverse for the sub node :" ;
	errs()<< subNode->getFunction()->getName() << "\n";
      }
      //FIXME move here?
    }
    if (vis[subNode] == 0){
      if (subNode != NULL && subNode->getFunction() != NULL && subNode->getFunction()->isDeclaration() != true){
	if(DEBUG){
	  errs() << "do dfsTraverse for the sub node :" ;
	  errs()<< subNode->getFunction()->getName() << "\n";
	}
	dfsTraverse(subNode);
      }
    }
 }
}


void MyCGraph::generateCallGraph(CallGraph &callgraph, Function* fn)
{

// traverse the call graph
//   int count = 0;
//   for (CallGraph::const_iterator i = callgraph.begin(); i != callgraph.end(); ++i) {
//   
//     errs() << count++ <<"\n";
//     auto test = i->second.get();
//     test->dump();
//     
//   }

//NOTE here I use DFS to traverse all the related functions
//     below line is to get the call graph node for the given function
CallGraphNode* cgNode =  callgraph.operator[](fn);  

//clear the visit map before do the DFS
vis.clear();
dfsTraverse(cgNode);

for(auto test : order){
  if(test != NULL && test->getFunction() != NULL){
    if(DEBUG){
      errs() << "print the final result!";
      errs() << test->getFunction()->getName() << "\n";
    }
    relatedFnNameSet.insert(test->getFunction()->getName());
  }
}


/*
 *      std::set<Function*> related_fns;
        // so, we can use the DFS
        //NOTE below line is to get the call graph node for the given function
        CallGraphNode* testNode =  callgraph.operator[](fn);
        errs() << "now just dump the node\n";
        errs() << "size ====" << testNode->size() <<"\n";
        testNode->dump();

	errs()<< "testNode getfuntion's name === " << testNode->getFunction()->getName() << "\n";

	int maxsize = testNode->size();
	for (int i = 0; i < maxsize; i++){
	  CallGraphNode * test = testNode->operator[](i);
	  
	  int size = test->size();
	  errs() << "second size ====" << size <<"\n";
	  for(int k = 0; k < size; k++){
	    CallGraphNode * test2 = test->operator[](k);
	    errs()<< "test2 getfuntion's name === " << test2->getFunction()->getName() << "\n";
	  }
	  
	  //related_fns.insert(test->getFunction());
	  errs()<< "test getfuntion's name === " << test->getFunction()->getName() << "\n";
	}
	*/

}


void MyCGraph::checkZigZag(CallGraph& callgraph, std::set< Function* > annotated_fns)
{
//NOTE here I use DFS to traverse all the related functions
//     below line is to get the call graph node for the given function

  for(auto fn : annotated_fns){
    if(fn == NULL) continue;

    CallGraphNode* cgNode =  callgraph.operator[](fn);
    vis.clear();
    order.clear();
    dfsTraverse(cgNode);

    // "order" set contains all root function's callees
    int sign = 0;
    for(auto callee : order){
      if(callee == NULL || callee->getFunction() == NULL){
	continue;
      }
      if (annotated_fns.find(callee->getFunction()) != annotated_fns.end()){
	sign++;
      }
    }
    // only if all callee is the annotated function
    // then the caller function (root function in the call graph) can be partition
    // add the root function to the non-zigzag function set
    if (sign == order.size()){
      noZigzagFnNameSet.insert(fn->getName());      
    } else {
      zigzagFnNameSet.insert(fn->getName());
    }
   
  }
  vis.clear();
  order.clear();
}



/*
 * analyze each instruction of functions in the module
 * sign whether the function contained or modified global variables
 */
void MyCGraph::analyzeGlobalVar(llvm::Module &m)
{
  // will find the global variable and update this set during traverse
  std::set<std::string>globalVarName;

  //m.getGlobalList();
  //m.getGlobalVariable();

    for (auto &fn : m) {
      //if(fn.getName()!="main")continue;
        for (auto &basic_block : fn) {
            for (auto &inst : basic_block) {
	        llvm::Instruction* testInst = &inst;
	        //errs()<< "function's name ==== " << testInst->getFunction()->getName() <<"\n";
		//how to print the instruction -- AsmWriter.cpp
		//void AssemblyWriter::printInstruction(const Instruction &I)
		std::string testStr;
		raw_string_ostream OS(testStr);
		testInst->print(OS);
		StringRef compareStr(OS.str());

		for(auto name : gvNameSet) {
		    // here we can check the given name, and see whether it contains in the instruction
		    // in short, if the function contains the global variable, then we wouldn't partition it
		    if(compareStr.contains(name)){
		        errs()<< "can I use this tricy way?" << OS.str() << "\n";
			errs()<< "function's name ==== " << testInst->getFunction()->getName() <<"\n";
			//if(the function name is not in the secure function set)
		    }
		}
            }
        }
    }
}



std::string MyCGraph::toNormalType(std::string IRType)
{
	      std::string type;
	      if(IRType == "i32"){
		type = "int";
	      } else if (IRType == "i32*"){
		type = "int*";
	      } else if (IRType == "i16"){
		type = "short";
	      } else if (IRType == "i16*"){
		type = "short*";
	      } else if (IRType == "i64"){
		type = "long";
	      } else if (IRType == "i64*"){
		type = "long*";
	      } else if (IRType == "i8"){
		type = "char";
	      } else if (IRType == "i8*"){
		type = "char*";
	      } else {
		type = IRType;
	      }

return type;
}


std::string MyCGraph::toNormalGlobalVarType(std::string IRType)
{
  std::string type;
  type = IRType.substr(0, (IRType.size()-1));
  return toNormalType(type);
}



char MyCGraph::ID = 0;
static RegisterPass<MyCGraph>
Y("mycgraph", "my Call Graph");
