#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "llvm/Support/raw_ostream.h"

//#include "../common/common.h"

// TA part:

// other functions
#define ERRX "errx"
#define MEMSET "memset"
#define MEMCPY "memcpy"
#define SIZEOF "sizeof"

// other header files
#define err_h "#include <err.h>"
#define stdio_h "#include <stdio.h>"
#define stdlib_h "#include <stdlib.h>"
#define string_h "#include <string.h>"

#define DEF "#define "
#define STATIC "static"

#define RPC_CLIENT_INCLUDE "RPC_CLIENT_INCLUDE.h"
#define RPC_SERVER_INCLUDE "RPC_SERVER_INCLUDE.h"
#define RPC_CLIENT "RPC_ClientStub.cpp"
#define RPC_SERVER "RPC_ServerStub.cpp"

#include "../util/common.h"

#include "llvm/Support/Error.h"

static int uuid = 0;

class OPTEEFunction {
private:

public:

	void construct_Read_Basic(std::ostringstream &ostr);
	void construct_Read_ByteArray(std::ostringstream &ostr);
	void construct_Read_ByteArrayBack(std::ostringstream &ostr);
	void construct_Write_Basic(std::ostringstream &ostr);
	void construct_Write_ByteArray(std::ostringstream &ostr);

	void write_basic(std::ostringstream &ostr, struct ParamDecl paramDecl);
	void write_bytearray(std::ostringstream& ostr, struct ParamDecl paramDecl);
	void read_basic(std::ostringstream& ostr, struct ParamDecl paramDecl);
	void read_bytearray(std::ostringstream& ostr, struct ParamDecl paramDecl);
	void read_bytearrayback(std::ostringstream& ostr, struct ParamDecl paramDecl);


	void mashallingParams(std::ostringstream& ostr, int &argNum,
	                      std::vector<struct ParamDecl> &paramDecls);


	void rewriteParams(std::ostringstream& ostr, std::string fnName, int& argNum,
	                   std::vector<ParamDecl>& paramDecls);


	void write_struct(std::ostringstream& ostr, struct ParamDecl paramDecl);
	void read_struct(std::ostringstream& ostr, struct ParamDecl paramDecl);

	void writeReturnValue(std::ostringstream &ostr, std::string fnName,
	                      std::string returnType, int argNum,
	                      std::vector<struct ParamDecl> &paramDecls);

	int createUUID();

	void construct_server_stub_Fn(std::ostringstream &ostr, std::string fnName,
	                              std::string returnType, int argNum,
	                              std::vector<struct ParamDecl> paramDecls);

	void construct_client_stub_Fn(std::ostringstream &ostr, std::string fnName,
	                              std::string returnType, int argNum,
	                              std::vector<struct ParamDecl> paramDecls);
	///////////////////////////////////////////////////////////

	void construct_struct_HeaderFiles(std::ostringstream &ostr, std::set<struct FunctionInfo> mFunctions);
	void construct_declaration_HeaderFiles(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions);
	void construct_function_HeaderFiles(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions);

	void construct_client_Includes(std::ostringstream &ostr, std::set<struct GlobalVarDecl> gvSet);
	void construct_server_Includes(std::ostringstream &ostr, std::set<struct GlobalVarDecl> gvSet);

	void construct_ClientHeaderDefine(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions);
	void construct_ServerHeaderDefine(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions);




};


void OPTEEFunction::construct_struct_HeaderFiles(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions)
{
	ostr << "\n";
	for (auto &fInfo : mFunctions) {
		for (auto &pDecl : fInfo.paramDecls) {
			struct StructInfo structInfo = pDecl.structInfo;
			if (structInfo.name.empty()) continue;
			ostr << "struct ";
			ostr << structInfo.name;
			ostr << " {\n";
			for (auto ipair : structInfo.items) {
				ostr <<  ipair.second << " "
				     <<  ipair.first << ";" << "\n";
			}
			ostr << "\n};\n";
		}
	}
}

void OPTEEFunction::construct_declaration_HeaderFiles(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions)
{

       ostr << "\n";
	for (auto &fInfo : mFunctions) {
		std::vector<struct ParamDecl> decls = fInfo.paramDecls;
		int argNum = fInfo.argNum;
		ostr << "extern " << fInfo.ReturnType << " " << fInfo.DemangledName << "(";
		for (int i = 0; i < argNum; i++) {
			struct ParamDecl paramDecl = decls.at(i);
			ostr << paramDecl.Type << " " << paramDecl.Name;
			if ( i != (argNum - 1)) {
				ostr << ", ";
			}
		}
		ostr << ")" ";" "\n";
	}
       
}
void OPTEEFunction::construct_function_HeaderFiles(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions)
{
	ostr << "\n";
	for (auto &fInfo : mFunctions)
	{
		std::vector<struct ParamDecl> paramDecls = fInfo.paramDecls;
		int argNum = fInfo.argNum;
		ostr << "struct ";

		if (fInfo.isMangling)
			ostr << fInfo.DemangledName;
		else
			ostr << fInfo.Name;
		ostr << "{\n";
		ostr << " int size;\n";
		ostr << " char* funcName;\n";
		for (int i = 0; i < argNum; ++i) {
			struct ParamDecl paramDecl = paramDecls.at(i);
			ostr << " " << (paramDecl).Type;
			ostr << " " << (paramDecl).Name << ";";
			ostr << "\n";
		}
		if (fInfo.ReturnType != "void")
		{
			ostr << " " << fInfo.ReturnType << " ret;\n";
		}
		ostr << "};\n";
	}
	

}


//void OPTEEFunction::construct_Host_HeaderFiles(std::ostringstream &ostr,
//		std::set<std::string> gvNameUsedSet, std::set<std::string> gvTypeUsedSet) {
void OPTEEFunction::construct_server_Includes(std::ostringstream &ostr, std::set<struct GlobalVarDecl> gvSet) {

	ostr << "\n";
	ostr << "#include \"RPC_SERVER_INCLUDE.h\"" << "\n";
	for (auto gv : gvSet) {
		ostr << "extern" << " " << gv.Type << " " << gv.Name << ";" << "\n";
	}
}

void OPTEEFunction::construct_client_Includes(std::ostringstream &ostr, std::set<struct GlobalVarDecl> gvSet) {
	ostr << "\n";
	ostr << "#include \"RPC_CLIENT_INCLUDE.h\"" << "\n";
	for (auto gv : gvSet) {
		ostr << "extern" << " " << gv.Type << " " << gv.Name << ";" << "\n";
	}

}


void OPTEEFunction::construct_ClientHeaderDefine(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions)
{
	ostr << "#ifndef RPC_CLIENT_INCLUDE" << "\n"
	     << "#define RPC_CLIENT_INCLUDE" << "\n";
	ostr << string_h << "\n";
	ostr << stdlib_h << "\n";
   construct_struct_HeaderFiles(ostr, mFunctions);
   construct_function_HeaderFiles(ostr, mFunctions);
	ostr << "extern void* buffer();\n";
	ostr << "extern void  execute();\n";
	ostr << "#endif" << "\n";

}


void OPTEEFunction::construct_ServerHeaderDefine(std::ostringstream& ostr, std::set<struct FunctionInfo> mFunctions)
{
	ostr << "#ifndef RPC_SERVER_INCLUDE" << "\n"
	     << "#define RPC_SERVER_INCLUDE" << "\n";
	ostr << string_h << "\n";
	ostr << stdlib_h << "\n";
	construct_struct_HeaderFiles(ostr, mFunctions);
	construct_declaration_HeaderFiles(ostr,mFunctions);
    construct_function_HeaderFiles(ostr, mFunctions);
	ostr << "extern void* buffer();\n";
	ostr << "extern void  execute();\n";
	ostr << "#endif" << "\n";

}


void OPTEEFunction::construct_server_stub_Fn(std::ostringstream& ostr,
        std::string fnName, std::string returnType,
        int argNum, std::vector<struct ParamDecl> paramDecls) {

	ostr << "\n" "void handle_requist(";
	ostr << ")" << " " << " {" << "\n";
	//let's try it only using one function
	ostr << "struct " << fnName << "*" << "ptr" << " = " << "(struct " << fnName << "*)" << "(unsigned int *)" << "buffer();"
	     << ";" "\n";
	//if (argNum != 0) {
		/*rewrite the synchronized param back*/
		//rewriteParams(ostr, fnName, argNum, paramDecls);
	//}
	writeReturnValue(ostr, fnName, returnType, argNum, paramDecls);
	ostr << "ptr->ret = ret;";
	ostr << "execute();";
	ostr << "};";

}


void OPTEEFunction::construct_client_stub_Fn(std::ostringstream& ostr,
        std::string fnName, std::string returnType,
        int argNum, std::vector<struct ParamDecl> paramDecls) {
	ostr << "\n" << returnType << " " << "RPC_" << fnName << "(";
	for (int i = 0; i < argNum; ++i) {
		struct ParamDecl paramDecl = paramDecls.at(i);
		ostr << " " << (paramDecl).Type;
		ostr << " " << (paramDecl).Name;
		if (i < argNum - 1)
			ostr << ",";
	}

	ostr << ")" << " " << " {" << "\n";
    ostr << "\n";

	ostr << "struct " << fnName << "*" << " ptr" << " = " << "(struct " << fnName << "*)" << "(unsigned int *)" <<
	     "buffer()" << ";\n";

	if (argNum != 0) {
		/*rewrite the synchronized param back*/
		//mashallingParams(ostr, argNum, paramDecls);
	}
	ostr << "execute();\n";

	if (returnType != "void")
		ostr << "return " << "ptr->ret" << ";" << "\n";

	ostr << "}" "\n";
}









void OPTEEFunction::rewriteParams(std::ostringstream& ostr, std::string fnName, int& argNum,
                                  std::vector<ParamDecl>& paramDecls) {
	//FIXME here need to check whether the parameter needs to synchronize or not
	// just write those parameters to the buffer
// 	ostr << "move = 0; \n";
// 	ostr << "len = 0; \n";

	for (int i = 0; i < argNum; ++i) {
		struct ParamDecl paramDecl = paramDecls.at(i);
		std::string type = paramDecl.Type;
		if (type.find('*') != std::string::npos) {
			//NOTE use read_bytearrayback to rewrite the params
			//read_bytearray(ostr, paramDecl);
			read_bytearrayback(ostr, paramDecl);

		} else if (!paramDecl.structInfo.name.empty()) {
			read_struct(ostr, paramDecl);

		} else {
			read_basic(ostr, paramDecl);
		}
	}

}

void OPTEEFunction::mashallingParams(std::ostringstream& ostr, int& argNum,
                                     std::vector<struct ParamDecl> &paramDecls) {
	//FIXME I think we still need to check the argType first
	// since if the parameter is a pointer, we have to create a shared memory for it
	// does it mean that we only can have at most 4 pointers?
	// maybe we can give programmer the capablility (using annotation) to specify whether he wants to change the pointer/value or not
	// if the value need to be changed, then we need to allocate shared memory for the pointer
	// if not, we can just mashalling, and get the value during unmashlling

	ostr << "\n" << "int param_size" << " = ";
	for (int i = 0; i < argNum; ++i) {
		struct ParamDecl paramDecl = paramDecls.at(i);
		std::string type = paramDecl.Type;
		if (type.find('*') != std::string::npos) {
			//FIXME if paramDecl.size == 0, raise exception
			ostr << SIZEOF << "(" << "int" << ")";
			ostr << " + ";
			ostr << paramDecl.size;
		} else if (!paramDecl.structInfo.name.empty()) {
			//FIXME if paramDecl.size == 0, raise exception
			//struct case
			ostr << SIZEOF << "(" << "int" << ")";
			ostr << " + ";
			ostr << paramDecl.size;
		} else {
			ostr << SIZEOF << "(" << paramDecl.Type << ")";
		}
		if (i < argNum - 1)
			ostr << " + ";
	}
	ostr << ";" << "\n";
	// dealing with error

	// I'm going to start mashalling now!
	// 1. convert all the argument to byte array (maybe need to recalculate each size) --- no need
	// 2. connect each byte together by memcpy + move the pointer
// 	ostr << "int move = 0; \n";
// 	ostr << "int len = 0; \n";

	for (int i = 0; i < argNum; ++i) {
		struct ParamDecl paramDecl = paramDecls.at(i);
		std::string type = paramDecl.Type;


		if (type.find('*') != std::string::npos) {
			write_bytearray(ostr, paramDecl);

		} else if (!paramDecl.structInfo.name.empty()) {
			write_struct(ostr, paramDecl);

		} else {
			write_basic(ostr, paramDecl);
		}

	}

}


void OPTEEFunction::writeReturnValue(std::ostringstream& ostr,
                                     std::string fnName, std::string returnType,
                                     int argNum, std::vector<struct ParamDecl> &paramDecls) {
	if (returnType != "void") {
		ostr << returnType << " ret" << " = ";

		ostr << fnName << "(";
		for (int i = 0; i < argNum; i++) {
			struct ParamDecl paramDecl = paramDecls.at(i);
			ostr << "ptr->" << paramDecl.Name;
			if ( i != (argNum - 1)) {
				ostr << ", ";
			}

		}
		ostr << ")" ";" "\n";

		if (returnType.find('*') != std::string::npos) {


		} else {


		}

	} else {
		ostr << fnName << "(";
		for (int i = 0; i < argNum; i++) {
			struct ParamDecl paramDecl = paramDecls.at(i);
			ostr << "ptr->" << paramDecl.Name;
			if ( i != (argNum - 1))  {
				ostr << ", ";
			}

		}
		ostr << ")" ";" "\n";
	}

}


void OPTEEFunction::construct_Read_Basic(std::ostringstream& ostr)
{
	ostr << "\n";
	ostr << "static void " << "read_basic" << "(void* dest, unsigned int **src, int move)" << "{" << "\n";
	ostr << "memcpy(dest, *src, move);" << "\n";
	ostr << "*src += move;" << "\n";
	ostr << "}" << "\n";
}


void OPTEEFunction::construct_Read_ByteArray(std::ostringstream& ostr) {
	ostr << "\n";
	ostr << "static void read_bytearray(void** dest, unsigned int **src) {" << "\n";
	ostr << "int move = sizeof(int);" << "\n";
	ostr << "int len = 0;" << "\n";
	ostr << "memcpy(&len, *src, move);" << "\n";
	ostr << "*src += move;" << "\n";

	ostr << "move = len;" << "\n";
	ostr << "*dest = malloc(move);" << "\n";
	ostr << "memcpy(*dest, *src, move);" << "\n";
	ostr << "*src += move;" << "\n";
	ostr << "}" << "\n";
}

void OPTEEFunction::construct_Read_ByteArrayBack(std::ostringstream& ostr)
{
	ostr << "\n";
	ostr << "static void read_bytearrayback(void** dest, unsigned int **src) {" << "\n";
	ostr << "int move = sizeof(int);" << "\n";
	ostr << "int len = 0;" << "\n";
	ostr << "memcpy(&len, *src, move);" << "\n";
	ostr << "*src += move;" << "\n";

	ostr << "move = len;" << "\n";
	//NOTE if use malloc then the pointer will point to a new memeoy on heap
	//the original pointer var will not be changed
	//ostr << "*dest = malloc(move);"<< "\n";
	ostr << "memcpy(*dest, *src, move);" << "\n";
	ostr << "*src += move;" << "\n";
	ostr << "}" << "\n";
}


void OPTEEFunction::construct_Write_Basic(std::ostringstream& ostr)
{
	ostr << "\n";
	ostr << "static void write_basic(unsigned int **p, void* src, int move) {" << "\n";
	ostr << "memcpy(*p, src, move);" << "\n";
	ostr << "*p += move;" << "\n";
	ostr << "}" << "\n";
}


void OPTEEFunction::construct_Write_ByteArray(std::ostringstream& ostr) {
	ostr << "\n";
	ostr << "static void write_bytearray(unsigned int **p, void* src, int len) {"  << "\n";
	ostr << "int move = sizeof(int);" << "\n";
	ostr << "memcpy(*p, &len, move);" << "\n";
	ostr << "*p += move;" << "\n";

	ostr << "move = len;" << "\n";
	ostr << "memcpy(*p, src, move);" << "\n";
	ostr << "*p += move;" << "\n";
	ostr << "}" << "\n";
}

void OPTEEFunction::write_basic(std::ostringstream& ostr, struct ParamDecl paramDecl)
{
	ostr << "write_basic" << "("
	     << "&" << "ptr->" << paramDecl.Name << ", "
	     << "&" << paramDecl.Name << ", "
	     << SIZEOF << "(" << paramDecl.Type << ")"
	     << ")"
	     << ";" << "\n";
}

void OPTEEFunction::write_bytearray(std::ostringstream& ostr, struct ParamDecl paramDecl)
{
	ostr << "write_bytearray" << "("
	     << "&" << "ptr->" << paramDecl.Name << ", "
	     << paramDecl.Name << ", "
	     << paramDecl.size
	     << ")"
	     << ";" << "\n";
}

void OPTEEFunction::write_struct(std::ostringstream& ostr, struct ParamDecl paramDecl)
{
	string structVarName = paramDecl.Name;
	for (auto ipair : paramDecl.structInfo.items) {
		llvm::StringRef type(ipair.second);
		string name = structVarName;
		name.append(".");
		name.append(ipair.first);
		if (!type.contains("*")) {
			ostr << "write_basic" << "("
			     << "&" << "ptr->" << paramDecl.Name << ", "
			     << "&" << name << ", "
			     << SIZEOF << "(" << type.str() << ")"
			     << ")"
			     << ";" << "\n";
		} else {
			ostr << "write_bytearray" << "("
			     << "&" << "ptr->" << paramDecl.Name << ", "
			     << name << ", "
			     //FIXME here need the size, maybe add a size field into struct, like pair<string,string,int>, the 3th is size
			     // now just hardcode here
			     << "100"

			     << ")"
			     << ";" << "\n";
		}
	}
}


void OPTEEFunction::read_basic(std::ostringstream& ostr, struct ParamDecl paramDecl)
{
	ostr << "read_basic" << "("
	     << "&" << paramDecl.Name << ", "
	     << "&" << "ptr" << ", "
	     << SIZEOF << "(" << paramDecl.Type << ")"
	     << ")"
	     << ";" << "\n";
}


void OPTEEFunction::read_struct(std::ostringstream& ostr, struct ParamDecl paramDecl)
{
	string structVarName = paramDecl.Name;
	for (auto ipair : paramDecl.structInfo.items) {
		llvm::StringRef type(ipair.second);
		string name = structVarName;
		name.append(".");
		name.append(ipair.first);
		if (!type.contains("*")) {
			ostr << "read_basic" << "("
			     << "&" << name << ", "
			     << "&" << "ptr" << ", "
			     << SIZEOF << "(" << type.str() << ")"
			     << ")"
			     << ";" << "\n";
		} else {
			ostr << "read_bytearray" << "("
			     << "&" << name << ", "
			     << "&" << "ptr"
			     << ")"
			     << ";" << "\n";
		}
	}
}


void OPTEEFunction::read_bytearrayback(std::ostringstream& ostr, ParamDecl paramDecl)
{
	ostr << "read_bytearrayback" << "("
	     << "&" << paramDecl.Name << ", "
	     << "&" << "ptr"
	     << ")"
	     << ";" << "\n";
}



// generate unique id, maybe use the SHA-256 or current time later
int OPTEEFunction::createUUID() {
	return uuid++;
}
