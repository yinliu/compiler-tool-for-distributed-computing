#ifndef RPCGenerator_HPP__
#define RPCGenerator_HPP__

#include <set>
#include <fstream>
#include <sstream>

#include <sys/stat.h>
#include <sys/types.h>
#include <stdio.h>

#include "OP-TEE_def.h"
#include "FunctionAnalysis.hpp"
#include "../util/common.h"

#include "../util/Annotation.hpp"
#include "../util/Demangling.hpp"
#include "../util/Struct.hpp"

using namespace std;
using namespace llvm;

// create directories for the generated stub
const char* clientPath = "./rpc_client";
const char* serverPath = "./rpc_server";
int isCreateHost = 0;
int isCreateTa = 0;



class RPCGenerator {
private:
	Module* mModule;
	std::ofstream sf;
	std::ostringstream ostr;
	//int uuid = 0;
	OPTEEFunction opTeeFunc;
	//std::set<Function*> mSecureFnSet;
	std::set<struct FunctionInfo> mFunctions;

	bool haveStruct = false;

	//FIXME need to use vector later to deal with multi-global variables case
	std::set<struct GlobalVarDecl> gvSet;
	// below is obsoleted
	std::set<std::string> gvNameUsedSet;
	std::set<std::string> gvTypeUsedSet;

	//std::string returnType;
	// function arg name
	//std::vector<std::string> argName;
	// function arg type
	//std::vector<std::string> argType;
	// function arg num
	//int argNum;

	//std::vector<struct ParamDecl> paramDecls;

	//Function * mFn;
	//std::string mFnName;

	//bool needDemangling = false;

	int RPC_Server_HeaderFileGenerator();
	int RPC_Client_HeaderFileGenerator();
	int RPC_Server_StubGenerator();
	int RPC_Client_StubGenerator();

	int createFolder();

	int RPCPreparation(Function *fD);

public:
	RPCGenerator(Function *fD);
	RPCGenerator(std::set<struct FunctionInfo> &preparedFunctionSet, Module* M, bool strustStatus);

	void generate_fn(std::set<Function*> &secureFnSet, Module* M);
	int GenerateRPC();
	void setGlobalVarInfo(std::set<std::string> nameSet, std::set<std::string> typeSet);
};

void RPCGenerator::generate_fn(std::set<Function*> &secureFnSet, Module* M) {
	mModule = M;
	//mSecureFnSet = secureFnSet;
	for (auto &f : secureFnSet) {
		RPCPreparation(f);
	}
}

RPCGenerator::RPCGenerator(set< FunctionInfo >& preparedFunctionSet, Module* M, bool strustStatus)
{
	mModule = M;
	//mFunctions = preparedFunctionSet;
	haveStruct = strustStatus;

	// for gvNameUsedSet and gvTypeUsedSet
	for (auto fnInfo : mFunctions) {
		if (!fnInfo.hasGlobalVars) continue;
		for (auto gv : fnInfo.globalVarDecls) {
			gvSet.insert(gv);
		}
	}
}



int RPCGenerator::RPCPreparation(Function* fn)
{
	struct FunctionInfo fInfo = {};
	fInfo.Name = (fn->getName()).str();
	StringRef fnNameRef(fInfo.Name);
	if (fnNameRef.contains("_Z")) {
		fInfo.isMangling = true;
		fInfo.DemangledName = demanglingName(fInfo.Name);
		//errs() << "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG" << '\n';
	}
	else
	{
		fInfo.DemangledName = (fn->getName()).str();
	}

	FunctionAnalysis fnAnalysis(fn);
	std::string fnReturnType;
	fnAnalysis.getReturnType(fnReturnType);
	fInfo.ReturnType = fnReturnType;

	//check the result
	int fnArgnum = 0;
	std::vector<std::string> fnArgName;
	std::vector<std::string> fnArgType;
	fnAnalysis.getArgInfo(fnArgnum, fnArgName, fnArgType);
	fInfo.argNum = fnArgnum;

	for (int i = 0; i < fnArgnum; i++) {
		struct ParamDecl decl = {};
		if (fInfo.isMangling)
			decl.fnName = fInfo.DemangledName;
		else
			decl.fnName = fInfo.Name;

		//decl.Name = fnArgName.at(i);
		StringRef argName(fnArgName.at(i));
		if (!argName.empty()
		        && (argName.contains(".")
		            || (argName.contains("coerce"))
		           )) {
			decl.Name = demanglingStructVarName(argName.str());
		} else {
			decl.Name = fnArgName.at(i);
		}


		StringRef argTypeName(fnArgType.at(i));
		errs() << "nnnnnnnMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM" << argTypeName << "\n";

		if (!argTypeName.empty()
		        && (argTypeName.contains("%")
		            || argTypeName.contains(".")
		            || argTypeName.contains("struct"))) {
			// for the struct case
			// TODO maybe extract all the parameters info from metadata???
			// FIXME here will be the C++ case that need to investigate later
			decl.Type = demanglingStructTypeName(argTypeName.str());

		} else if (argTypeName.contains("[")
		           && argTypeName.contains("]")
		           && argTypeName.contains("x")) {
			// for the struct case
			// TODO maybe extract all the parameters info from metadata???
			struct StructInfo structInfo = getStructType(*mModule, decl.fnName);
			string typeName = "Struct ";
			typeName.append(structInfo.name);
			decl.Type = typeName;
			decl.structInfo = structInfo;
			haveStruct = true;
		}
		else {
			decl.Type = fnArgType.at(i);
		}

		decl.size = getParamlen(*fn, decl.Name);
		fInfo.paramDecls.push_back(decl);
	}

	mFunctions.insert(fInfo);

}

/*
RPCGenerator::RPCGenerator(Function* fn) {

	this->mFn = fn;
	//std::string fnName = (fn->getName()).str();
	this->mFnName = (fn->getName()).str();
	StringRef fnNameRef(mFnName);

	if (fnNameRef.contains("_Z")) {
	  this->needDemangling = true;
	}

	FunctionAnalysis fnAnalysis(fn);

	//check the result
	  int fnArgnum = 0;
	  std::vector<std::string> fnArgName;
	  std::vector<std::string> fnArgType;

	  std::string fnReturnType;
	  fnAnalysis.getReturnType(fnReturnType);
	  fnAnalysis.getArgInfo(fnArgnum, fnArgName, fnArgType);

	  this->argNum = fnArgnum;
	  this->argName = fnArgName;
	  this->argType = fnArgType;
	  this->returnType = fnReturnType;

	  errs()<< "fnReturnType == " << fnReturnType <<"\n";
	  errs()<< "fnArgnum == " << fnArgnum <<"\n";

	  for(int i = 0; i < fnArgnum; ++i) {
	    errs()<< "fnArgName == " << fnArgName.at(i) <<"\n";
	    errs()<< "fnArgType == " << fnArgType.at(i) <<"\n";
	  }

	std::vector<int> fnArgSize;
	fnAnalysis.getSize(fnArgSize);

	for (int i = 0; i < fnArgnum; i++) {
		struct ParamDecl decl = { };
		//decl.fnName = fnName;
		if (this->needDemangling)
		  decl.fnName = demanglingName(this->mFnName);
		else
		  decl.fnName = this->mFnName;

		decl.Name = fnArgName.at(i);

		StringRef argTypeName(fnArgType.at(i));
		if (!argTypeName.empty()
		    && (argTypeName.contains("%")
		    || argTypeName.contains(".")
		    || argTypeName.contains("struct"))) {

		  decl.Type = demanglingStructName(argTypeName.str());
		 } else {
		   decl.Type = fnArgType.at(i);
		 }

		//decl.size = fnArgSize.at(i);
		//FIXME just for now, make it to "100"
		decl.size = getParamlen(*fn, decl.Name);
		paramDecls.push_back(decl);
	}

}*/

int RPCGenerator::GenerateRPC() {
// 	std::string fnName;
// 	if (this->needDemangling)
// 	  fnName = demanglingName(this->mFnName);
// 	else
// 	  fnName = this->mFnName;

	int isCreateSuccessful = createFolder();

	//FIXME we set time only when we use it
	//opTeeFunc.setCommand(fnName);
	//opTeeFunc.setWapperFnName(fnName);
	///////////////////////////////////

	RPC_Client_HeaderFileGenerator();
	RPC_Server_HeaderFileGenerator();
	RPC_Server_StubGenerator();
	RPC_Client_StubGenerator();
}

//2.1 generate RPC include
int RPCGenerator::RPC_Server_HeaderFileGenerator() {

	if (!isCreateTa) {
		std::string dPath(serverPath);
		sf.open(dPath + "/" + RPC_SERVER_INCLUDE,
		        ios::out | ios::app);
	} else {
		sf.open(RPC_SERVER_INCLUDE, ios::out | ios::app);
	}

	opTeeFunc.construct_ServerHeaderDefine(ostr, mFunctions);
	sf << ostr.str() << endl;
	sf.close();
	ostr.str("");
	//ostr.clear();
}

int RPCGenerator::RPC_Client_HeaderFileGenerator() {

	if (!isCreateTa) {
		std::string dPath(clientPath);
		sf.open(dPath + "/" + RPC_CLIENT_INCLUDE,
		        ios::out | ios::app);
	} else {
		sf.open(RPC_CLIENT_INCLUDE, ios::out | ios::app);
	}
	opTeeFunc.construct_ClientHeaderDefine(ostr, mFunctions);
	sf << ostr.str() << endl;
	sf.close();
	ostr.str("");
	//ostr.clear();

}

int RPCGenerator::RPC_Server_StubGenerator() {
	if (!isCreateTa) {
		std::string dPath(serverPath);
		sf.open(dPath + "/" + RPC_SERVER,
		        ios::out | ios::app);
	} else {
		sf.open(RPC_SERVER, ios::out | ios::app);
	}

	if (sf.is_open()) {
		opTeeFunc.construct_server_Includes(ostr, gvSet);
		// add struct structure


		// add read/write function///////////////
		opTeeFunc.construct_Read_Basic(ostr);
		opTeeFunc.construct_Read_ByteArray(ostr);
		opTeeFunc.construct_Write_Basic(ostr);
		opTeeFunc.construct_Write_ByteArray(ostr);

		// for rewrite the pointer value
		opTeeFunc.construct_Read_ByteArrayBack(ostr);

		/////////////////////////////////////////
		// traverse all the annotated function and generate the stub
		for (auto &fInfo : mFunctions) {
			std::string func_name = fInfo.isMangling ? fInfo.DemangledName : fInfo.Name;
			if (func_name.compare("multiplay") == 0)
			{
				opTeeFunc.construct_server_stub_Fn(ostr, func_name, fInfo.ReturnType, fInfo.argNum,
				                                   fInfo.paramDecls);
			}
		}
		sf << ostr.str() << endl;
		sf.close();
		ostr.str("");
		//ostr.clear();
	}
}

//2.2 generate client_stub
int RPCGenerator::RPC_Client_StubGenerator() {
	if (!isCreateTa) {
		std::string dPath(clientPath);
		sf.open(dPath + "/" + RPC_CLIENT,
		        ios::out | ios::app);
	} else {
		sf.open(RPC_CLIENT, ios::out | ios::app);
	}

	if (sf.is_open()) {
		opTeeFunc.construct_client_Includes(ostr, gvSet);
		// add struct structure

		// add read/write function///////////////
		opTeeFunc.construct_Read_Basic(ostr);
		opTeeFunc.construct_Read_ByteArray(ostr);
		opTeeFunc.construct_Write_Basic(ostr);
		opTeeFunc.construct_Write_ByteArray(ostr);

		// for rewrite the pointer value
		opTeeFunc.construct_Read_ByteArrayBack(ostr);

		/////////////////////////////////////////
		// traverse all the annotated function and generate the stub
		for (auto &fInfo : mFunctions) {
			std::string func_name = fInfo.isMangling ? fInfo.DemangledName : fInfo.Name;
			opTeeFunc.construct_client_stub_Fn(ostr, func_name, fInfo.ReturnType, fInfo.argNum,
			                                   fInfo.paramDecls);
		}
		sf << ostr.str() << endl;
		//ostr.clear();
		sf.close();
		ostr.str("");
	}

	// clear the OutputStreams
}


int RPCGenerator::createFolder() {
	//create directories for TA and Host
	isCreateHost = mkdir(serverPath, S_IRWXU | S_IRWXG | S_IRWXO);
	if (isCreateHost) {
		printf("create path failed! error code : %d %s \n", isCreateHost, serverPath);
		return -1;
	}

	isCreateTa = mkdir(clientPath, S_IRWXU | S_IRWXG | S_IRWXO);

	if (isCreateTa) {
		printf("create path failed! error code : %d %s \n", isCreateTa, clientPath);
		return -1;
	}
	return 1;
}


void RPCGenerator::setGlobalVarInfo(set< string > nameSet, set< string > typeSet)
{
	this->gvNameUsedSet = nameSet;
	this->gvTypeUsedSet = typeSet;
}



#endif // RPCGenerator_HPP__
