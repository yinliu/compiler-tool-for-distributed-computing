#ifndef Report_HPP__
#define Report_HPP__

#include <fstream>
#include <set>
#include <sstream>

#include "../util/common.h"

using namespace std;
using namespace llvm;

#define REPORTADDR "report"

class Report {
private:
  ofstream sf;
  ostringstream ostr;
  std::set<struct FunctionInfo> mFnInfoSet;

  void prepare();
  void release();

public:
  Report(std::set<struct FunctionInfo> fnInfoSet){
    mFnInfoSet = fnInfoSet;
  }

  void report();
};

void Report::prepare()
{
  sf.open(REPORTADDR, ios::out | ios::app);
}

void Report::release()
{
  if(sf && sf.is_open())
    sf.close();
  
  if(ostr)
    ostr.str("");
}

void Report::report()
{
  prepare();
  int num = 1;
  for(struct FunctionInfo fnInfo : mFnInfoSet){
      ostr << num++ << "; "
           << "Name: " << fnInfo.Name << "; "
	   << "is ZigZag: " << fnInfo.isZigZag << "; "
	   << "has global vars: " << fnInfo.hasGlobalVars << "; "
	   << "can be partitioned: "<< fnInfo.canBePartition << "; "
	   << "\n";
  }

  sf << ostr.str() << endl;
  release();
}





#endif