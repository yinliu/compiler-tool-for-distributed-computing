//===- RPC.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "RPC World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "partitionFunc"


#include "llvm/Pass.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
// #include "../util/Demangling.hpp"
// #include "../util/Annotation.hpp"
// #include "../util/Partition.hpp" // need to be after the Demangling.hpp

#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/Bitcode/BitcodeWriterPass.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/Regex.h"
#include "llvm/Support/Signals.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Transforms/IPO.h"

#include "../util/Demangling.hpp"
#include "../util/Annotation.hpp"
#include "../util/Refactoring.hpp"
#include "../util/Struct.hpp"
#include "../util/Persistence.hpp"



//#include "../cpipass/MyCallGraph.hpp"
//#include "../cpipass/OP-TEE_def.h"
#include "../cpipass/RPCGenerator.hpp"
//#include "../cpipass/RPCReplace.hpp"
//#include "../cpipass/Report.hpp"
//#include "FunctionAnalysis.hpp"



#include <memory>


#include "llvm/Transforms/Utils/Cloning.h"



using namespace llvm;
using namespace std;

extern Module *module;

namespace {
// Replace - The second implementation with getAnalysisUsage implemented.
struct partitionFunc : public ModulePass {
	static char ID; // Pass identification, replacement for typeid

	// The option for how to extract (partition) the functions on IR level
	bool DeleteFn = false;
	bool OutputAssembly = true;
	bool PreserveAssemblyUseListOrder = false;
	bool PreserveBitcodeUseListOrder = false;
	bool Force = false;

	std::set<struct FunctionInfo> staFnInfoSet = {};

	partitionFunc() : ModulePass(ID) {}

	virtual bool runOnModule(llvm::Module &m) {

		//WARNING below line cannot work
		//llvm::Module originalModule(m.getName(), m.getContext());
		// NOTE correct approach to clone a module:
		std::unique_ptr<Module> originalModule = llvm::CloneModule(&m);

// 	// 1. get the annotated functions
//         // refer to http://bholt.org/posts/llvm-quick-tricks.html
//         errs() << "1. get the annotated functions start >>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << "\n";
// 	annoAnalysis(m, staFnInfoSet);
// 	//std::set<std::string> secure_fns_name;
//
// 	for(auto test: staFnInfoSet){
// 	  errs() << "test" << "\n";
// 	  errs() << "test name:" << test.Name << "\n";
// 	  errs() << "test isPatition:" << test.isPatition << "\n";
// 	  errs() << "test exetime.deadline:" << test.exetime.deadline << "\n";
// 	  errs() << "test frequency.RT_Period:" << test.frequency.RT_Period << "\n";
// 	  errs() << "test rtLoop.LP_Counts:" << test.rtLoop.LP_Counts << "\n";
// 	  errs() << "test rtMem.RT_MemSize:" << test.rtMem.RT_MemSize << "\n";
// 	  errs() << "test rpc.Encryption:" << test.rpc.Encryption << "\n";
// 	  errs() << "test ectHandler.ExcTimes:" << test.ectHandler.ExcTimes << "\n";
// 	}
//
//         errs() << "1. get the annotated functions end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";
//
// 	// 2. get related functions by running our custom pass - MyCGraph
// 	errs() << "2. get related functions by running our custom pass start >>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << "\n";
// 	// find the function that annotated "partition"

//
// 	//MyCGraph* analysis = new MyCGraph();
// 	MyCGraph* analysis = new MyCGraph(partition_fns);
// 	analysis->runOnModule(m);
// 	errs() << "2. get related functions by running our custom pass end <<<<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";
//
// 	//just for test now
// 	std::set<std::string> zigzagFnNameSet =  analysis->zigzagFnNameSet;
// 	std::set<std::string> noZigzagFnNameSet =  analysis->noZigzagFnNameSet;
//
// 	errs()<< "zigzagFnNameSet: >>>>>>>>>>>>>>>>>>>>>>>>>>>." << "\n";
// 	for(auto fn : zigzagFnNameSet){
// 	  errs()<< fn << "\n";
// 	}
// 	errs()<< "zigzagFnNameSet <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";
// 	errs()<< "nozigzagFnNameSet: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << "\n";
// 	for(auto fn : noZigzagFnNameSet){
// 	  errs()<< fn << "\n";
// 	}
// 	errs()<< "nozigzagFnNameSet <<<<<<<<<<<<<<<<<<<<<<<<<<<, " << "\n";

		//4. partition it via the llvm pass
		//TODO extract it to a function
		//TODO consider multiple files, convert all the file to one .bc file first?
		// -- we can do that, llvm-link all the files to one .bc first
		errs() << "4. partition it via the llvm pass start >>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << "\n";
		// collect the function that can be partitioned
		//std::set<Function*> partition_fns;
		Persistence psn;
		std::set<struct FunctionInfo> fnInfoSet = psn.readback();

		std::set<std::string> partitionFunctionSet;
		for (auto fnInfo : fnInfoSet) {
			if (fnInfo.canBePartition) {
				partitionFunctionSet.insert(fnInfo.Name);
			}
		}

		std::set<Function*> partition_fns = {};
		for (auto fnInfo : fnInfoSet) {
			if (fnInfo.isPatition) {
				StringRef strRefFnName(fnInfo.Name);
				Function* tempFn = m.getFunction(strRefFnName);
				partition_fns.insert(tempFn);

			}
		}

		// Use SetVector to avoid duplicates.
		SetVector<GlobalValue *> GVs;

		for (auto &f : m) {
			std::string name = f.getName();

			// the GV has to only contain the variables should be remove from the IR level
			if (partitionFunctionSet.find(name) == partitionFunctionSet.end())
				continue;

			//partition_fns.insert(&f);
			GlobalValue *GV = &f;
			GVs.insert(GV);
		}

		ExitOnError ExitOnErr(std::string("TEST") + ": error reading input: ");

		auto Materialize = [&](GlobalValue & GV) { ExitOnErr(GV.materialize()); };

		// Materialize requisite global values.
		// NOTE materilize is for solve the lazy loading problem
		// refer to : getParamlen
		// https://stackoverflow.com/questions/45642228/what-does-materialize-mean-in-llvm-globalvalue-h/45802504#45802504
		if (!DeleteFn) {
			for (size_t i = 0, e = GVs.size(); i != e; ++i)
				Materialize(*GVs[i]);
		} else {
			// Deleting. Materialize every GV that's *not* in GVs.
			SmallPtrSet<GlobalValue *, 8> GVSet(GVs.begin(), GVs.end());
			for (auto &F : m) {
				if (!GVSet.count(&F))
					Materialize(F);
			}
		}


		{
			std::vector<GlobalValue *> Gvs(GVs.begin(), GVs.end());
			legacy::PassManager Extract;
			errs() << "\n before add GVpass \n";
			ModulePass * test = llvm::createGVExtractionPass(Gvs, DeleteFn);
			Extract.add(test);
			errs() << "\n after add GVpass \n";

			Extract.run(m);

			// Now that we have all the GVs we want, mark the module as fully
			// materialized.
			// FIXME: should the GVExtractionPass handle this?
			ExitOnErr(m.materializeAll());
		}

		// In addition to deleting all other functions, we also want to spiff it
		// up a little bit.  Do this now.
		legacy::PassManager Passes;

		if (!DeleteFn) {
			Passes.add(createGlobalDCEPass());           // Delete unreachable globals
		}
		Passes.add(createStripDeadDebugInfoPass());    // Remove dead debug info
		Passes.add(createStripDeadPrototypesPass());   // Remove dead func decls

		std::error_code EC;
		tool_output_file Out("server.bc", EC, sys::fs::F_None);
		if (EC) {
			errs() << EC.message() << '\n';
			return 1;
		}

		if (OutputAssembly)
			Passes.add(
			    createPrintModulePass(Out.os(), "", PreserveAssemblyUseListOrder));
		else if (Force || !CheckBitcodeOutputToConsole(Out.os(), true))
			Passes.add(createBitcodeWriterPass(Out.os(), PreserveBitcodeUseListOrder));

		Passes.run(m);

		// Declare success.
		// NOTE we must call the function - "keep" here, otherwise it will be automatically deleted
		Out.keep();
		errs() << "4. partition it via the llvm pass end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";

// 	//5. generate the stub
		errs() << "5. generate the stub start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << "\n";

		module = originalModule.get();

		/** FIXME here the logic is wrong, we need to consider the case of multiple annotated function
		 * when the num of annotated function is more than one
		 * 1. host side:
		 *    - original function need to change all functions' name to "RPC_" + original name (now it should work)
		 *    - Client_Stub.c traverse the secure_fns, generate the RPC_xxx body for each function ---ok
		 *
		 * 2. ta side:
		 *    - partitioned.bc, need to partition all the related function (now it might work, need to check the call graph analysis part)
		 *    - RPC_myCriticalFunction_Includes.h
		 *            need to generate the command id for each annotated function like (#define TA_myCriticalFunction_CMD_WRAPPER 0) --- ok
		 *            need to fix its name, don't need to generate that header file for each annotated function --- ok
		 *    - user_ta_header_defines.h
		 *            change the include file to the above fixed name --- ok
		 *    - test_function_call_ta.c
		 *            change include header file to the above fixed name --- ok
		 *            in "TA_InvokeCommandEntryPoint" we need a new case --- ok
		 *            generate new xxx_WRAPPER --- ok
		 *
		 * 3. both in host and ta side, we need to add the release part
		 *
		 */

		// function info preparing  staFnInfoSet analysis->fnInfoSet
// 	 std::set<struct FunctionInfo> preparedFunctionSet;
// 	 bool haveStruct = false;
// 	 for(auto fnInfo: analysis->fnInfoSet){
// 	  if (!fnInfo.canBePartition) continue;
// 	  for(auto annoFnInfo : staFnInfoSet){
// 	    if (fnInfo.Name != annoFnInfo.Name) continue;
// 	    struct FunctionInfo combineInfo = {};
// 	    combineInfo.Name = fnInfo.Name;
//
// 	    //1. for the infos of static analysis (check whether it can be partitioned)
// 	    combineInfo.isZigZag = fnInfo.isZigZag;
// 	    combineInfo.hasGlobalVars = fnInfo.hasGlobalVars;
// 	    combineInfo.canBePartition = fnInfo.canBePartition;
// 	    combineInfo.globalVarDecls = fnInfo.globalVarDecls;
// 	    combineInfo.reportInfo = fnInfo.reportInfo;
//
// 	    //2. for the infos of annotations analysis (obtained its all annotations)
// 	    combineInfo.isPatition = annoFnInfo.isPatition;
// 	    combineInfo.exetime = annoFnInfo.exetime;
// 	    combineInfo.frequency = annoFnInfo.frequency;
// 	    combineInfo.rtLoop = annoFnInfo.rtLoop;
// 	    combineInfo.rtMem = annoFnInfo.rtMem;
// 	    combineInfo.rpc = annoFnInfo.rpc;
// 	    combineInfo.ectHandler = annoFnInfo.ectHandler;
//
// 	    //3. for preparating the rest of information
//             StringRef fnNameRef(fnInfo.Name);
// 	    Function* fn = module->getFunction(fnNameRef);
//
// 	    FunctionAnalysis fnAnalysis(fn);
// 	    std::string fnReturnType;
// 	    fnAnalysis.getReturnType(fnReturnType);
// 	    combineInfo.ReturnType = fnReturnType;
//
// 	    //check the result
// 	    int fnArgnum = 0;
// 	    std::vector<std::string> fnArgName;
// 	    std::vector<std::string> fnArgType;
// 	    fnAnalysis.getArgInfo(fnArgnum, fnArgName, fnArgType);
// 	    combineInfo.argNum = fnArgnum;
//
// 	    for (int i = 0; i < fnArgnum; i++) {
// 	      struct ParamDecl decl = {};
// 		decl.fnName = combineInfo.Name;
//
// 		//decl.Name = fnArgName.at(i);
// 		StringRef argName(fnArgName.at(i));
// 		if(!argName.empty()
// 		  && (argName.contains(".")
// 		    || (argName.contains("coerce"))
// 		  )){
// 		  decl.Name = demanglingStructVarName(argName.str());
// 		} else {
// 		  decl.Name = fnArgName.at(i);
// 		}
//
// 		StringRef argTypeName(fnArgType.at(i));
// 		if(!argTypeName.empty()
// 		      && (argTypeName.contains("%")
// 			      || argTypeName.contains(".")
// 			      || argTypeName.contains("struct"))) {
// 		    // for the struct case
// 		    // TODO maybe extract all the parameters info from metadata???
// 		    // FIXME here will be the C++ case that need to investigate later
// 		    decl.Type = demanglingStructTypeName(argTypeName.str());
//
// 		} else if(argTypeName.contains("[")
// 			      && argTypeName.contains("]")
// 			      && argTypeName.contains("x")){
// 		  // for the struct case
// 		  // TODO maybe extract all the parameters info from metadata???
// 		  struct StructInfo structInfo = getStructType(*module, decl.fnName);
// 		  string typeName = "Struct ";
// 		  typeName.append(structInfo.name);
// 		  decl.Type = typeName;
// 		  decl.structInfo = structInfo;
// 		  haveStruct = true;
// 		}
// 		else {
// 		    decl.Type = fnArgType.at(i);
// 		}
//
// 		decl.size = getParamlen(*fn, decl.Name);
// 		combineInfo.paramDecls.push_back(decl);
// 	    }
//             preparedFunctionSet.insert(combineInfo);
// 	  }
// 	 }

		bool haveStruct = false;
		for (auto fnInfo : fnInfoSet) {
			if (fnInfo.hasStruct)
				haveStruct = true;
		}


		RPCGenerator rpcGenerator(fnInfoSet, module, haveStruct);
		rpcGenerator.generate_fn(partition_fns, module);
		rpcGenerator.GenerateRPC();
		//RPCGenerator rpcGenerator(fnInfoSet, module, haveStruct);
		//generate RPC stub
		//rpcGenerator.GenerateRPC();

		//RPCGenerator rpcGenerator(partition_fns, module);
		//generate RPC stub
		//rpcGenerator.setGlobalVarInfo(analysis->gvNameUsedSet, analysis->gvTypeUsedSet);
		//rpcGenerator.GenerateRPC();

		return true;
	}

	// We don't modify the program, so we preserve all analyses.
	virtual void getAnalysisUsage(AnalysisUsage &AU) const {
		//AU.setPreservesAll();
	}

};

}

char partitionFunc::ID = 0;
static RegisterPass<partitionFunc>
X("ptn", "partitionFunc Pass");
