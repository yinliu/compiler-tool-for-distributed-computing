//===- RPC.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "RPC World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "rpc_transform"
#include "llvm/Pass.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/FileSystem.h"


#include "../util/Annotation.hpp"
#include "../util/Demangling.hpp"
#include "../util/Persistence.hpp"
#include "../util/common.h"

#include <set>
using namespace llvm;

namespace {
// RPCReplace - The second implementation with getAnalysisUsage implemented.
struct RPCTransform : public ModulePass {
    static char ID; // Pass identification, replacement for typeid
    RPCTransform() : ModulePass(ID) {}

    virtual bool runOnModule(llvm::Module &m) {
        bool changed = false;
        for (auto &funct : m) {
            for (auto &basic_block : funct) {
                std::vector<Instruction *> toberemoved;
                for (auto &inst : basic_block) {
                    /* We can't remove the instruction while iterating because it
                     * would invalidate the iterator
                     */
                    bool needtoremove = handleCallInstruction(m, CallSite(&inst));
                    if (needtoremove)
                        toberemoved.push_back(&inst);
                    changed |= needtoremove;
                }
                for (Instruction * i : toberemoved)
                    i->eraseFromParent();
            }
        }


        errs() << "after replacing :\n";
        //NOTE dump the IR code for checking the replace results
        //m.dump();
        //string errorMessage = "coudn't stream the module to client";
        std::error_code EC;
        raw_fd_ostream file("client.bc", EC, sys::fs::F_None); 
        m.print(file, NULL);
        file.close();
        errs() << "RPCTransform runOnModule -----------------" << "\n";
        return true;
    }
    Function * reportThisCall;
    std::set<std::string> secure_fns_name;
public:
    void setAnnotatedFnName(std::set<std::string> name);

private:
    bool handleCallInstruction(llvm::Module &module, llvm::CallSite cs) {
        auto callInstruction = cs.getInstruction();
        // Check whether the instruction is actually a call - why is this necessary !???!?!?!
        if (!callInstruction) {
            return false;
        }
        // Check whether the called function is directly invoked
        // this I believe is where virtual calls would need to be handled somehow.
        auto fun = dyn_cast<Function>(cs.getCalledValue()->stripPointerCasts());
        if (!fun) {
            return false;
        }
        // replace the call to kkk with a
        // call _reportThisCall("k_k_k", 42, <followed by original args>)
        // here will first get the annotation later
//         if (fun->getName() == "kkk") {
        std::string fnName = fun->getName();
        if (secure_fns_name.find(fnName) != secure_fns_name.end()) {
            /*build the RPC function*/
            // this would declare 'void _reportThisCall(const char *, int);'
            //reportThisCall = cast<Function>(m.getOrInsertFunction("_reportThisCall", Type::getVoidTy(m.getContext()), strType, intType, nullptr));
            //
            // this declares a function 'void _reportThisCall(const char *, int, ...);'
//      auto intType = Type::getInt32Ty(module.getContext());
//      auto strType = Type::getInt8PtrTy(module.getContext());
//      auto ftype = FunctionType::get(Type::getVoidTy(module.getContext()),
//          {strType, intType}, /* variadic = */ true);
            auto ftype = cs.getFunctionType();
            std::string rpcNameStr = "RPC_" + fnName;
            StringRef rpcName(rpcNameStr);
            reportThisCall = cast<Function>(module.getOrInsertFunction(rpcName, ftype));
            IRBuilder<> builder(callInstruction);
            //auto fname = builder.CreateGlobalStringPtr("k_k_k");
            //auto val = ConstantInt::get(Type::getInt32Ty(module.getContext()), 42);
            //std::vector<Value *> args = {fname};
            //args.push_back(val);

            std::vector<Value *> args = {};
            for (auto arg = cs.arg_begin(); arg != cs.arg_end(); arg++) {
                args.push_back(*arg);
            }
            builder.CreateCall(reportThisCall, args);
            return true;
        }

        
        return false;

    }
    virtual bool runOnFunction(Function &F) {
        errs() << "RPC: ";
        errs().write_escaped(F.getName()) << '\n';
        return false;
    }
    // We don't modify the program, so we preserve all analyses.
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
        AU.setPreservesAll();
    }
};
void RPCTransform::setAnnotatedFnName(std::set< std::string > name)
{
    this->secure_fns_name = name;
}
}

char RPCTransform::ID = 0;
static RegisterPass<RPCTransform>
Z("rpt", "RPCTransform Pass");
