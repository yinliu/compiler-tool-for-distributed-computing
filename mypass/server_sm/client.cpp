/*
 * Skeleton files for a shared memeory server
 *
 * @author Kibur Girum
 */


#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <semaphore.h>
#include <getopt.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <cstring>

using namespace std;
extern int getValue();

/*
 */
string MEME_ID = "";
string SNAME = "";
int MEME_SIZE = 1024;
string CLIENT_SEM = "client";

struct Employee {
    int age, sal, yrs;
    char posn;
};

/*
 * A non-concurrent, iterative server that serves one client at a time.
 * For each client, it handles exactly 1 HTTP transaction.
 */
static void
client_loop()
{
   getValue();     
}
static void
usage(char * av0)
{
    perror("Usage: [-s semphore name] [-i memeory Id] [n]\n");
    exit(EXIT_FAILURE);
}

int
main(int ac, char *av[])
{
    int opt;
    while ((opt = getopt(ac, av, "s:di:dn:")) != -1) {
        switch (opt) {
        case 's':
            SNAME = optarg;
            break;
        case 'i':
            MEME_ID = optarg;
            break;
        case 'n':
            MEME_SIZE = atoi(optarg);
            break;
        default:    /* '?' */
            usage(av[0]);
        }
    }

     if (MEME_ID.size() == 0 || SNAME.size() == 0)
    {
        cout << "Error: please specify the memeory ID, size and the semphore name for the server\n";
        usage(av[0]);
        exit(EXIT_FAILURE);
    }
    if (MEME_SIZE <= 0)
    {
        MEME_SIZE = 1024;
    }
    cout << "Using simphore name %s\n" << SNAME;
    client_loop();
    exit(EXIT_SUCCESS);
}

void* buffer()
{
   // ftok to generate unique key
    
    char *cstar = new char[MEME_ID.length() + 1];
    strcpy(cstar, MEME_ID.c_str());
    key_t key = ftok(cstar,65);
    free(cstar);
    int shmid = shmget(key,MEME_SIZE,0666|IPC_CREAT);
    char *str = (char*) shmat(shmid,(void*)0,0);
    return str;

}

void execute()
{
    
    char *cstar = new char[CLIENT_SEM.length() + 1];
    strcpy(cstar, CLIENT_SEM.c_str());
    sem_unlink(cstar);
    sem_t *semClient = sem_open(cstar, O_CREAT, 0644, 1);
    sem_trywait(semClient);
    cstar = new char[SNAME.length() + 1];
    strcpy(cstar, SNAME.c_str());
    sem_t *sem = sem_open(cstar, 0);
    if (sem_post(sem) == -1 )
    {
        perror("Error: problem accessing server semaphore");
        free(cstar);
        exit(EXIT_FAILURE);
    }
    free(cstar);
    sem_wait(semClient);
}


void destroyShMem()
{
   char *cstar = new char[MEME_ID.length() + 1];
   strcpy(cstar, MEME_ID.c_str());
   key_t key = ftok(cstar,65);
   int shmid = shmget(key,MEME_SIZE,0666|IPC_CREAT);
   char *str = (char*) shmat(shmid,(void*)0,0);
   shmdt(str);
   shmctl(shmid,IPC_RMID,NULL);
}

