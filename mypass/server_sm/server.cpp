/*
 * Skeleton files for a shared memeory server
 *
 * @author Kibur Girum
 */


#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <semaphore.h>
#include <getopt.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <cstring>

using namespace std;
extern void handle_requist();

/*
 */
const string MEME_ID  = "server";
int MEME_SIZE = 0;
string SNAME = "";
string CLIENT_SEM = "client";
char* servPointer;

//static void * thread_helper(void * args) {
    
            //pthread_exit(NULL)
//}
/*
 * A non-concurrent, iterative server that serves one client at a time.
 * For each client, it handles exactly transaction.
 */

 void* buffer()
{
    return servPointer;
}

int execute()
{
    char* cstar = new char[CLIENT_SEM.length() + 1];
    strcpy(cstar, CLIENT_SEM.c_str());
    sem_t *sem = sem_open(cstar, 0);
    if (sem_post(sem) == -1 )
    {
        perror("Error: problem accessing client semaphore");
        free(cstar);
        exit(EXIT_FAILURE);
        return -1;
    }
    return 0;
}


static void
server_loop()
{

    char *cstar = new char[SNAME.length() + 1];
    strcpy(cstar, SNAME.c_str());
    sem_unlink(cstar);

    sem_t *sem = sem_open(cstar, O_CREAT, 0644, 1);
    sem_trywait(sem);
    free(cstar);
    cstar = new char[MEME_ID.length() + 1];
    strcpy(cstar, MEME_ID.c_str());
    key_t key = ftok(cstar,65);
    free(cstar);
    int shmid = shmget(key, MEME_SIZE,0666|IPC_CREAT);
    if (shmid == -1)
    {
        perror("Error: coundn't allocate memeory for the server. plese try again\n");
        exit(EXIT_FAILURE);
    }
    cout << "memeory id to access the server: " << MEME_ID << "\n"; 
    char *str = (char*) shmat(shmid,(void*)0,0);
    cout << "waiting for a client........\n";
    sem_wait(sem);

    cout << "client accepted !\n";
    cout << "Writeing Data.....\n";
    servPointer = str;
    handle_requist();
    cout << "yessssssssssssssssss";
    // pthread_t threads[NUM_THREADS];
    // int i;
    // for (i = 0; i < NUM_THREADS; i++) {
    //     pthread_create(&threads[i], NULL, thread_helper, independent_copy);
    // }
    // for (i = 0; i < NUM_THREADS; i++) {
    //     pthread_join(threads[i], NULL);
    // }
    // char *cstar = new char[SNAME.length() + 1];
    // strcpy(cstar, SNAME.c_str());
    // sem_unlink(cstar);
    
    // sem_t *sem = sem_open(cstar, O_CREAT, 0644, 1);
    // sem_trywait(sem);
    // free(cstar);
    // cstar = new char[MEME_ID.length() + 1];
    // strcpy(cstar, MEME_ID.c_str());
    // key_t key = ftok(cstar,65);
    // free(cstar);
    // int shmid = shmget(key, MEME_SIZE,0666|IPC_CREAT);
    // if (shmid == -1)
    // {
    //     perror("Error: coundn't allocate memeory for the server. plese try again\n");
    //     exit(EXIT_FAILURE);
    // }
    // cout << "memeory id to access the server: " << MEME_ID << "\n"; 
    // char *str = (char*) shmat(shmid,(void*)0,0);
    // cout << "waiting for a client........\n";
    // sem_wait(sem);
    // cout << "client accepted !\n";
    // Employee* emp = (Employee*) str;
    // calculate(emp);
    // cout <<"Writeing Data.....\n";
    // cout<< emp->sal << "\n";
    // shmdt(str);
    // shmctl(shmid,IPC_RMID,NULL);
    // sem_post(sem);

}


static void
usage(char * av0)
{
    perror("Usage: [-s semphore name] [-n memeory size]");
    exit(EXIT_FAILURE);
}

int
main(int ac, char *av[])
{
    int opt;
    while ((opt = getopt(ac, av, "s:dn:d")) != -1) {
        switch (opt) {
        case 's':
            SNAME = optarg;
            break;
        case 'n':
            MEME_SIZE = atoi(optarg);
            break;
        default:    /* '?' */
            usage(av[0]);
        }
    }
    if (MEME_SIZE <= 0 || SNAME.size() == 0)
    {
        cout << "Error: please specify the memeory size and the semphore name for the server\n";
        usage(av[0]);
        exit(EXIT_FAILURE);
    }

    cout << "Using simphore name: " << SNAME << "\n";
    server_loop();
    exit(EXIT_SUCCESS);
}



