#serv.cpp
	-This is the source file for the server. 
	-To compile use: g++ -o servPosix -pthread -lrt -std=c++11 serv.cpp
	-This implementation uses POSIX shared memory and spawns a thread to run the call to calculate

#cli.cpp
	-This is the source file for the client.
	-To compile use: g++ -o cliPosix -pthread -lrt -std=c++11 cli.cpp
	
#servPosix
	-Usage: [-s semaphoreName -n memorySize]
	-Run the server first and when it will notify user when it is blocked, so client can be run

#cliPosix
	-Usage: [-s semaphoreName -n memorySize -i memoryID]
	-Start the client once server has been blocked on the semaphore
