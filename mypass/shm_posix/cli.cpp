#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "employee.h"

using namespace std;

int openMem(char*, void**, int);
void usage();

int main(int argc, char* argv[]) {
	char* semName, *memId;
	void* memPtr;
	int memSz, opt;
	sem_t* sem;
	while ((opt = getopt(argc, argv, "s:i:n:")) != -1) {
		switch (opt) {
		case 's': 
			semName = optarg;
			break;	
		case 'n':
			memSz = atoi(optarg);
			break;	
		case 'i': 
			memId = optarg;
			break;
		default: 
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (openMem(memId, &memPtr, memSz) < 0) {
		cout << "Failed to open shared memory" << endl;
		exit(EXIT_FAILURE);
	}
	if ((sem = sem_open(semName, 0)) == SEM_FAILED) {
                perror("sem_open");
                exit(EXIT_FAILURE);
        }
	if (memSz < sizeof(Employee)) {
		cout << "Not enough space in shared memory to store Employee!" << endl;
		exit(EXIT_FAILURE);
	}
	cout << "Writing data to shared memory!" << endl;
	Employee* e = reinterpret_cast<Employee*>(memPtr);
	cout << "Enter employee age: ";
	cin >> (*e).age;
	cout << "\nEnter employee years experience: ";
	cin >> e->yrs;
	cout << "\nEnter employee's position code(e, s, or p): ";
	cin >> e->posn;
	cout << "\nData read in, releasing semaphore.." << endl;
	if (sem_post(sem) < 0) {
		perror("sem_post");
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}

int openMem(char* memName, void** memPtr, int memSz) {
	int fd;
	if ((fd = shm_open(memName, O_RDWR, S_IRUSR | S_IWUSR)) < 0) {
		perror("shm_open");
		return -1;
	}
	if ((*memPtr = mmap(NULL, memSz, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED) {
		perror("mmap");
		return -1;
	}
	cout << "Successfully opened shared memory object: " << memName << endl;
	return 0;
}

void usage() {
	cout << "Usage: [-s semaphoreName -n memorySize -i memoryId]" << endl;
}
