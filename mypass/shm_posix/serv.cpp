#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <thread>
#include "employee.h"
#define MEM_ID "/calculate"

using namespace std;

int createMem(int, void**);
void usage();
int calculate(Employee*);
int createSem(sem_t*, char*);
int destMem(int, void*);

int main(int argc, char *argv[]) {
	char* semName;
	void* memPtr;
	int opt, memSz;
	sem_t* sem;
	while ((opt = getopt(argc, argv, "s:dn:d")) != -1) {
		switch (opt) {
		case 's': 
			semName = optarg;
			break;	
		case 'n':
			memSz = atoi(optarg);
			break;	
		default: 
			usage();
			exit(EXIT_FAILURE);
		}
	}
	if (createMem(memSz, &memPtr) < 0) {
		cout << "Failed to create the shared memory!" << endl;
		exit(EXIT_FAILURE);
	}
	if ((sem = sem_open(semName, O_CREAT, 0644, 1)) == SEM_FAILED) {
                perror("sem_open");
                exit(EXIT_FAILURE);
        }
        if(sem_trywait(sem) < 0) {
                perror("sem_trywait");
                exit(EXIT_FAILURE);
        }
	cout << "Blocking server on semaphore: " << semName << endl;
	if (sem_wait(sem) < 0) {
		perror("sem_wait");
		exit(EXIT_FAILURE);
	}
	cout << "Client has input data.. running calculate" << endl;
	Employee* e = reinterpret_cast<Employee*> (memPtr);
	cout << "Opening thread to run calculate.. " << endl;
	thread calc(calculate, e);
	cout << "Calling calculate" << endl;
	calc.join();
	cout << "Salary calculated to be: " << e->salary << endl;
	cout << "Done running calculate, removing semaphore and shared memory" << endl;
	if (sem_unlink(semName) < 0) {
		perror("sem_unlink");
		exit(EXIT_FAILURE);
	}
	if (destMem(memSz, reinterpret_cast<void*> (memPtr)) < 0) {
		cout << "Error removing shared memory" << endl;
		exit(EXIT_FAILURE);
	}
	exit(EXIT_SUCCESS);
}

int createMem(int size, void** memPtr) {
	int fd;
        if ((fd = shm_open(MEM_ID, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR)) < 0) {
                perror("shm_open");
                return -1;
        }
        if (ftruncate(fd, static_cast<unsigned int>(size)) < 0) {
                perror("ftruncate");
                return -1;
        }
	if ((*memPtr = mmap(NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0)) == MAP_FAILED) {
		perror("mmap");
		return -1;
	}
	close(fd);
	cout << "Shared memory created with id: " << MEM_ID << endl;
	return 0;
}

void usage() {
	cout << "Usage: [-s semaphoreName -n memorySize]" << endl;
}

int calculate(Employee* e) {
	if (e->posn == 'e') {
		e->salary = (e->yrs + e->age) * 10;
	}
	else if (e-> posn == 's') {
		e->salary = (e->yrs + e->age) * 20;
	}
	else {
		e->salary = (e->yrs + e->age) * 30;
	}
}

int destMem(int memSz, void* memPtr) {
	if (munmap(memPtr, memSz) < 0) {
		perror("munmap");
		return -1;
	}
	if (shm_unlink(MEM_ID) < 0) {
		perror("shm_unlink");
		return -1;
	}
	cout << "Shared memory unmapped and unlinked" << endl;
	return 0;
}
