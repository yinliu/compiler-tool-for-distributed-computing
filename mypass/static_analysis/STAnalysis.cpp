//===- RPC.cpp - Example code from "Writing an LLVM Pass" ---------------===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
// This file implements two versions of the LLVM "RPC World" pass described
// in docs/WritingAnLLVMPass.html
//
//===----------------------------------------------------------------------===//

#define DEBUG_TYPE "STAnalysis"


#include "llvm/Pass.h"
#include "llvm/IR/CallSite.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Transforms/IPO/PassManagerBuilder.h"
// #include "../util/Demangling.hpp"
// #include "../util/Annotation.hpp"
// #include "../util/Partition.hpp" // need to be after the Demangling.hpp

#include "llvm/Transforms/Utils/ValueMapper.h"
#include "llvm/ADT/SetVector.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/Bitcode/BitcodeWriterPass.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "llvm/IRReader/IRReader.h"
#include "llvm/IR/LegacyPassManager.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/Error.h"
#include "llvm/Support/FileSystem.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/PrettyStackTrace.h"
#include "llvm/Support/Regex.h"
#include "llvm/Support/Signals.h"
#include "llvm/Support/SourceMgr.h"
#include "llvm/Support/SystemUtils.h"
#include "llvm/Support/ToolOutputFile.h"
#include "llvm/Transforms/IPO.h"

#include "../util/Demangling.hpp"
#include "../util/Annotation.hpp"
#include "../util/Refactoring.hpp"
#include "../util/Struct.hpp"



#include "../cpipass/MyCallGraph.hpp"
//#include "OP-TEE_def.h"
//#include "../cpipass/RPCGenerator.hpp"
//#include "../cpipass/RPCReplace.hpp"
//#include "../cpipass/Report.hpp"
#include "../util/Persistence.hpp"
#include "../cpipass/FunctionAnalysis.hpp"



#include <memory>


#include "llvm/Transforms/Utils/Cloning.h"



using namespace llvm;
using namespace std;


namespace {
  // Replace - The second implementation with getAnalysisUsage implemented.
  struct STAnalysis : public ModulePass {
    static char ID; // Pass identification, replacement for typeid

    // The option for how to extract (partition) the functions on IR level
    bool DeleteFn = false;
    bool OutputAssembly = true;
    bool PreserveAssemblyUseListOrder = false;
    bool PreserveBitcodeUseListOrder = false;
    bool Force = false;

    std::set<struct FunctionInfo> staFnInfoSet = {};

    STAnalysis() : ModulePass(ID) {}

    virtual bool runOnModule(llvm::Module &m) {

	// 1. get the annotated functions
        // refer to http://bholt.org/posts/llvm-quick-tricks.html
        errs() << "1. get the annotated functions start >>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << "\n";
    errs() << "00000000000000000000000000000000000000000000111111111111111111111111111111:     :" << staFnInfoSet.size() << "\n";
	//std::set<Function*> secure_fns = getAnnotatedFn(m);
	annoAnalysis(m, staFnInfoSet);
	//std::set<std::string> secure_fns_name;
	 errs() << "00000000000000000000000000000000000000000000111111111111111111111111111111:     :" << staFnInfoSet.size() << "\n";

	for(auto test: staFnInfoSet){
	  errs() << "test" << "\n";
	  errs() << "test name:" << test.Name << "\n";
	  errs() << "test isPatition:" << test.isPatition << "\n";
	  errs() << "test exetime.deadline:" << test.exetime.deadline << "\n";
	  errs() << "test frequency.RT_Period:" << test.frequency.RT_Period << "\n";
	  errs() << "test rtLoop.LP_Counts:" << test.rtLoop.LP_Counts << "\n";
	  errs() << "test rtMem.RT_MemSize:" << test.rtMem.RT_MemSize << "\n";
	  errs() << "test rpc.Encryption:" << test.rpc.Encryption << "\n";
	  errs() << "test ectHandler.ExcTimes:" << test.ectHandler.ExcTimes << "\n";
	}

        errs() << "1. get the annotated functions end <<<<<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";

	// 2. get related functions by running our custom pass - MyCGraph
	errs() << "2. get related functions by running our custom pass start >>>>>>>>>>>>>>>>>>>>>>>>>>>>> " << "\n";
	// find the function that annotated "partition"
	std::set<Function*> partition_fns = {};
	for(auto fnInfo : staFnInfoSet){
	  if(fnInfo.isPatition){
	    StringRef strRefFnName(fnInfo.Name); 
	    Function* tempFn = m.getFunction(strRefFnName);
	    errs()<<"tempFn is ==== " << tempFn->getName() << "\n";
	    partition_fns.insert(tempFn);
	  }
	}
	//MyCGraph* analysis = new MyCGraph();
	MyCGraph* analysis = new MyCGraph(partition_fns);
	analysis->runOnModule(m);
	errs() << "2. get related functions by running our custom pass end <<<<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";

	//just for test now
	std::set<std::string> zigzagFnNameSet =  analysis->zigzagFnNameSet;
	std::set<std::string> noZigzagFnNameSet =  analysis->noZigzagFnNameSet;

	errs()<< "zigzagFnNameSet: >>>>>>>>>>>>>>>>>>>>>>>>>>>." << "\n";
	for(auto fn : zigzagFnNameSet){
	  errs()<< fn << "\n";
	}
	errs()<< "zigzagFnNameSet <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";
	errs()<< "nozigzagFnNameSet: >>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << "\n";
	for(auto fn : noZigzagFnNameSet){
	  errs()<< fn << "\n";
	}
	errs()<< "nozigzagFnNameSet <<<<<<<<<<<<<<<<<<<<<<<<<<<, " << "\n";

	//3. report the static analysis results
	errs()<< "report >>>>>>>>>>>>>>>>>>>>>>>>>>> " << "\n";
	//Report rep(analysis->fnInfoSet);
	//rep.report();
	errs()<< "report done <<<<<<<<<<<<<<<<<<<<<<<<<<< " << "\n";
	
	
	// 4. obtain all function infos
	 // function info preparing  staFnInfoSet analysis->fnInfoSet
	 std::set<struct FunctionInfo> preparedFunctionSet;
	 int id = 0;
	 bool haveStruct = false;
	 for(auto fnInfo: analysis->fnInfoSet){
	  if (!fnInfo.canBePartition) continue;
	  for(auto annoFnInfo : staFnInfoSet){
	    if (fnInfo.Name != annoFnInfo.Name) continue;
	    struct FunctionInfo combineInfo = {};
	    combineInfo.ID = id++;
        StringRef fnNRef(fnInfo.Name);
	    if (fnNRef.contains("_Z")) {
		combineInfo.isMangling = true;
		combineInfo.DemangledName = demanglingName(fnInfo.Name);
		//errs() << "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG" << '\n';
	    }
	    combineInfo.Name = fnInfo.Name;
	    //1. for the infos of static analysis (check whether it can be partitioned)
	    combineInfo.isZigZag = fnInfo.isZigZag;
	    combineInfo.hasGlobalVars = fnInfo.hasGlobalVars;
	    combineInfo.canBePartition = fnInfo.canBePartition;
	    combineInfo.globalVarDecls = fnInfo.globalVarDecls;
	    combineInfo.reportInfo = fnInfo.reportInfo;
	    
	    //2. for the infos of annotations analysis (obtained its all annotations)
	    combineInfo.isPatition = annoFnInfo.isPatition;
	    combineInfo.exetime = annoFnInfo.exetime;
	    combineInfo.frequency = annoFnInfo.frequency;
	    combineInfo.rtLoop = annoFnInfo.rtLoop;
	    combineInfo.rtMem = annoFnInfo.rtMem;
	    combineInfo.rpc = annoFnInfo.rpc;
	    combineInfo.ectHandler = annoFnInfo.ectHandler;
	    
	    //3. for preparating the rest of information
            StringRef fnNameRef(fnInfo.Name);
	    Function* fn = m.getFunction(fnNameRef);

	    FunctionAnalysis fnAnalysis(fn);
	    std::string fnReturnType;
	    fnAnalysis.getReturnType(fnReturnType);
	    combineInfo.ReturnType = fnReturnType;

	    //check the result
	    int fnArgnum = 0;
	    std::vector<std::string> fnArgName;
	    std::vector<std::string> fnArgType;
	    fnAnalysis.getArgInfo(fnArgnum, fnArgName, fnArgType);
	    combineInfo.argNum = fnArgnum;

	    for (int i = 0; i < fnArgnum; i++) {
	      struct ParamDecl decl = {};
		decl.fnName = combineInfo.Name;
		//decl.Name = fnArgName.at(i);
		StringRef argName(fnArgName.at(i));
		errs()<< "argName ========= " << argName << "\n";
		if(!argName.empty()
		  && (argName.contains(".")
		    || (argName.contains("coerce"))
		  )){
		  decl.Name = demanglingStructVarName(argName.str());
		} else {
		  decl.Name = fnArgName.at(i);
		}
		
		StringRef argTypeName(fnArgType.at(i));
		errs()<< "argTypeName ========= " << argTypeName << "\n";
		errs()<< " decl.fnName ===" <<  decl.fnName << "\n";
		if(!argTypeName.empty()
		      && (argTypeName.contains("%")
			      || argTypeName.contains(".")
			      || argTypeName.contains("struct"))) {
		    // for the struct case
		    // TODO maybe extract all the parameters info from metadata???
		    // FIXME here will be the C++ case that need to investigate later
		    decl.Type = demanglingStructTypeName(argTypeName.str());
		     errs()<< "decl.Type ===" << decl.Type << "\n";
		     // FIXME here need to add below lines, but now it doesn't work
		     // need to check the function --> getStructType
// 		     struct StructInfo structInfo = getStructType(m, decl.fnName);
// 		     string typeName = "Struct ";
// 		     typeName.append(structInfo.name);
// 		     decl.Type = typeName;
// 		     decl.structInfo = structInfo;
// 		     //haveStruct = true;
// 		     combineInfo.hasStruct = true;
		    
		} else if(argTypeName.contains("[")
			      && argTypeName.contains("]")
			      && argTypeName.contains("x")){
		  // for the struct case
		  // TODO maybe extract all the parameters info from metadata???
		  struct StructInfo structInfo = getStructType(m, decl.fnName);
		  string typeName = "Struct ";
		  typeName.append(structInfo.name);
		  decl.Type = typeName;
		  decl.structInfo = structInfo;
		  //haveStruct = true;
		  combineInfo.hasStruct = true;
		}
		else {
		    decl.Type = fnArgType.at(i);
		}
		decl.size = getParamlen(*fn, decl.Name);
		combineInfo.paramDecls.push_back(decl);
	    }
            preparedFunctionSet.insert(combineInfo);
	  }
	 }
	Persistence psn;
	psn.persist(preparedFunctionSet);

        return true;
    }

    // We don't modify the program, so we preserve all analyses.
    virtual void getAnalysisUsage(AnalysisUsage &AU) const {
      //AU.setPreservesAll();
    }

  };
  
}

char STAnalysis::ID = 0;
static RegisterPass<STAnalysis>
X("sta", "STAnalysis Pass");
