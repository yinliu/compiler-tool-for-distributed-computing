#include <iostream>
#include "Employee.h"

using namespace std;
/*
struct Employee {
	int age, sal, yrs;
	char posn;
};
*/
//__attribute__((annotate("server"))) void calculate(Employee);	
__attribute__((annotate("server")))  int  multiplay(int , int);
__attribute__((annotate("server")))  int add(int, int);

int getValue() {
	//Employee e;
	int a;
	int b;
	char c;
	cout << "Enter employee age: ";
	cin >> a;
	cout << "Enter employee's years with company: ";
	cin >> b;
	cout << "Enter employee's posn character code (e, s, or p): ";
	cin >> c;
	int sal = multiplay(a , b);
	cout << "Employee salary calculated to be $" << sal << "\n";
	return 0;
}

// void calculate(Employee e) {
// 	if (e.posn == 'e') {
// 		e.sal = e.age * e.yrs * 10;
// 	}	
// 	else if (e.posn == 's') {
// 		e.sal = e.age * e.yrs * 20;
// 	}
// 	else {
// 		e.sal = e.age * e.yrs * 30;
// 	}
// }

long clientMulti(int age , int yrs)
{
    return multiplay(age , yrs);
}

int clientAdd(int age, int yrs)
{
   return add(age, yrs);
}


int multiplay(int age , int yrs)
{
    return age * yrs;
}

int add(int num1, int num2)
{
	return  num1 + num2;
}
