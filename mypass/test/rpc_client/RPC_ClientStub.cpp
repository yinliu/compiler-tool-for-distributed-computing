
#include "RPC_CLIENT_INCLUDE.h"

static void read_basic(void* dest, unsigned int **src, int move){
memcpy(dest, *src, move);
*src += move;
}

static void read_bytearray(void** dest, unsigned int **src) {
int move = sizeof(int);
int len = 0;
memcpy(&len, *src, move);
*src += move;
move = len;
*dest = malloc(move);
memcpy(*dest, *src, move);
*src += move;
}

static void write_basic(unsigned int **p, void* src, int move) {
memcpy(*p, src, move);
*p += move;
}

static void write_bytearray(unsigned int **p, void* src, int len) {
int move = sizeof(int);
memcpy(*p, &len, move);
*p += move;
move = len;
memcpy(*p, src, move);
*p += move;
}

static void read_bytearrayback(void** dest, unsigned int **src) {
int move = sizeof(int);
int len = 0;
memcpy(&len, *src, move);
*src += move;
move = len;
memcpy(*dest, *src, move);
*src += move;
}

int RPC_add( int num1, int num2)  {

struct add* ptr = (struct add*)(unsigned int *)buffer();
execute();
return ptr->ret;
}

int RPC_multiplay( int age, int yrs)  {

struct multiplay* ptr = (struct multiplay*)(unsigned int *)buffer();
execute();
return ptr->ret;
}

