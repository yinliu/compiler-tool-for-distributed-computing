#ifndef RPC_SERVER_INCLUDE
#define RPC_SERVER_INCLUDE
#include <string.h>
#include <stdlib.h>


extern int add(int num1, int num2);
extern int multiplay(int age, int yrs);

struct add{
 int size;
 char* funcName;
 int num1;
 int num2;
 int ret;
};
struct multiplay{
 int size;
 char* funcName;
 int age;
 int yrs;
 int ret;
};
extern void* buffer();
extern void  execute();
#endif

