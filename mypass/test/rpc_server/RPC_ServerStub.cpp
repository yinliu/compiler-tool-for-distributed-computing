
#include "RPC_SERVER_INCLUDE.h"

static void read_basic(void* dest, unsigned int **src, int move){
memcpy(dest, *src, move);
*src += move;
}

static void read_bytearray(void** dest, unsigned int **src) {
int move = sizeof(int);
int len = 0;
memcpy(&len, *src, move);
*src += move;
move = len;
*dest = malloc(move);
memcpy(*dest, *src, move);
*src += move;
}

static void write_basic(unsigned int **p, void* src, int move) {
memcpy(*p, src, move);
*p += move;
}

static void write_bytearray(unsigned int **p, void* src, int len) {
int move = sizeof(int);
memcpy(*p, &len, move);
*p += move;
move = len;
memcpy(*p, src, move);
*p += move;
}

static void read_bytearrayback(void** dest, unsigned int **src) {
int move = sizeof(int);
int len = 0;
memcpy(&len, *src, move);
*src += move;
move = len;
memcpy(*dest, *src, move);
*src += move;
}

void handle_requist()  {
struct multiplay*ptr = (struct multiplay*)(unsigned int *)buffer();;
int ret = multiplay(ptr->age, ptr->yrs);
ptr->ret = ret;execute();};
