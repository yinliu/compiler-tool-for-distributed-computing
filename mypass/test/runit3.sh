
# opt must be from
# /opt/scratch/gback/rpc/llvm/llvm-install/bin/opt
SPTH='/home/kibur1/workspace/compiler-tool-for-distributed-computing/mypass/server_sm'
RPCC='/home/kibur1/workspace/compiler-tool-for-distributed-computing/mypass/test/rpc_client'
RPCS='/home/kibur1/workspace/compiler-tool-for-distributed-computing/mypass/test/rpc_server'
mkdir -p ./serverFolder
mkdir -p ./clientFolder

clang -g -S -c -emit-llvm calc.cpp -o calc1.bc
#clang -S -c -emit-llvm calc.cpp -o calc2.bc
 rm -rf rpc_client rpc_server
/home/kibur1/rpc/llvm/llvm-install/bin/opt -load ../build/static_analysis/libLLVMSTA.so -sta calc1.bc  
/home/kibur1/rpc/llvm/llvm-install/bin/opt -load ../build/partition_func/libLLVMPTN.so -ptn calc1.bc
/home/kibur1/rpc/llvm/llvm-install/bin/opt -load ../build/rpc_transform/libLLVMRPCTransform.so -rpt calc1.bc 
 
 clang -g -S -c -emit-llvm $SPTH/client.cpp -I/$SPTH/client.h -o ./clientFolder/client_sh.bc
 clang -g -S -c -emit-llvm $SPTH/server.cpp -I/$SPTH/server.h -o ./serverFolder/server_sh.bc

 clang -g -S -c -emit-llvm $RPCC/RPC_ClientStub.cpp -I/$RPCC/RPC_CLIENT_INCLUDE.h -o ./clientFolder/rpc_client.bc
 clang -g -S -c -emit-llvm $RPCS/RPC_ServerStub.cpp -I/$RPCS/RPC_SERVER_INCLUDE.h -o ./serverFolder/rpc_server.bc
  
cp server.bc ./serverFolder/
cp client.bc ./clientFolder/






# could also do
# opt -load ../build/callgraph/libLLVMCGraphDisplay.so -cgraph < callgraphtestmutualO0.bc | llvm-dis -
