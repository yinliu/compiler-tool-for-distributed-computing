#ifndef Annotation_HPP__
#define Annotation_HPP__


#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
//#include <llvm/IR/Constants.h>
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/BasicBlock.h"
//#include <../../../LLVM/llvm/include/llvm/IR/BasicBlock.h>
#include <set>
#include <iostream>

#include "common.h"
#include "StrOperation.hpp"
using namespace llvm;

Module *module;

std::string getAnnotationString(std::string name)
{
	// assumption: the zeroth operand of a Value::GlobalVariableVal is the actual Value
	Value *v = module->getNamedValue(name)->getOperand(0);
	ConstantDataArray *ca = (ConstantDataArray *)v;
	// errs() << "result nb =======" << ca->getAsCString() <<'\n';
	return ca->getAsCString();
}

int getAnnotationInt(std::string name)
{
	// assumption: the zeroth operand of a Value::GlobalVariableVal is the actual Value
	Value *v = module->getNamedValue(name)->getOperand(0);
	ConstantDataArray *ca = (ConstantDataArray *)v;
	std::string ca_string = ca->getAsCString();

	return std::stoi(ca_string);
}


void dumpLoCInfo(llvm::Module &m) {
	module = &m;
	for (auto &funct : m) {
		for (auto &basic_block : funct) {
			StringRef bbName(basic_block.getName());
			errs() << "BasicBlock: "  << bbName << "\n";
			std::string bbNameStr = bbName.str();
			std::string secureBlockName = "secure_block";
			if (bbNameStr.empty() || bbNameStr != secureBlockName) continue;
			errs() << "secure block: "  << bbNameStr << "\n";
			int i = 0;
			for (auto &inst : basic_block) {
				Instruction *I = &inst;

				//errs() << "i = " << i++ << '\n';
				//errs() << inst << '\n';
				int num_operands = inst.getNumOperands();
				errs() << num_operands << '\n';
				//errs() <<  << '\n';
				//errs() << I->getNumOperands() << '\n';
				if (num_operands == 0) continue;
				errs() << I->getOpcodeName() << '\n';


				for (int j = 0; j < num_operands; j++) {
					Value *Op = inst.getOperand(j);
					//errs() << "operand is " << Op->getValueName() << '\n';
					errs() << "inst is \n";
					errs() << inst << '\n';

					errs() << "Op is \n" ;
					Op->dump();

					std::string OpName = Op->getName().str();
					errs() << "operand is " << OpName << '\n';

					if (OpName.empty()) {

					}

					std::string strType;
					llvm::raw_string_ostream osType(strType);
					Op->getType()->print(osType);
					std::string temp_type = osType.str();

					errs() << "operand type is " << temp_type << '\n';
				}


				Value *annotation_lable = inst.getOperand(inst.getNumOperands() - 1);
				if (annotation_lable->getName().str() != "llvm.var.annotation")continue;
				errs() << inst << '\n';

				// get annotated obj
				Value *annotatedValue = inst.getOperand(0);
				Value *annotated_variable = cast<Instruction>(annotatedValue)->getOperand(0);
				errs() << "annotatedValue ==" << *annotatedValue << '\n';
				errs() << "name is ==" << annotated_variable->getName().str() << '\n';
				StringRef varName(annotated_variable->getName());

				std::string strType;
				llvm::raw_string_ostream osType(strType);
				annotated_variable->getType()->print(osType);
				std::string temp_type = osType.str();

				errs() << "!!!!!!!!!!!! type =======" << temp_type << "\n";

				//Type varType(annotated_variable->getType()->getInt32PtrTy());
				//Type testType =  annotated_variable->getType();

				//get annotation
				//https://stackoverflow.com/questions/4976298/modern-equivalent-of-llvm-annotationmanager
				Value *annotation = inst.getOperand(1);
				ConstantExpr *ce = (ConstantExpr *)annotation;
				// `ConstantExpr` operands: http://llvm.org/docs/LangRef.html#constantexprs
				Value *gv = ce->getOperand(0);
				errs() << "gv->getName() ==" << gv->getName().str() << '\n';
				errs() << "result ==" << getAnnotationString(gv->getName().str()) << '\n';
			}
		}
	}
}





/*
std::string getAnnotationString(llvm::Module &m, std::string name)
{
    // assumption: the zeroth operand of a Value::GlobalVariableVal is the actual Value
    Value *v = m.getNamedValue(name)->getOperand(0);
    ConstantDataArray *ca = (ConstantDataArray *)v;
   // errs() << "result nb =======" << ca->getAsCString() <<'\n';
    return ca->getAsCString();
}


int getAnnotationInt(llvm::Module &m, std::string name)
{
    // assumption: the zeroth operand of a Value::GlobalVariableVal is the actual Value
    Value *v = m.getNamedValue(name)->getOperand(0);
    ConstantDataArray *ca = (ConstantDataArray *)v;
   // errs() << "result nb =======" << ca->getAsCString() <<'\n';
    return ca->getAsSignedInteger();
}*/

void dumpAnnotatedObj(llvm::Module &m) {
	for (auto &funct : m) {
		for (auto &basic_block : funct) {
			for (auto &inst : basic_block) {
				Instruction *I = &inst;
				// errs() << "i = " << i++ << '\n';
				// errs() << inst << '\n';moudle
				int num_operands = inst.getNumOperands();
				//errs() <<  << '\n';
				//errs() << I->getNumOperands() << '\n';
				if (num_operands == 0)continue;

				Value *annotation_lable = inst.getOperand(inst.getNumOperands() - 1);
				if (annotation_lable->getName().str() != "llvm.var.annotation")continue;
				errs() << inst << '\n';

				// get annotated obj
				Value *annotatedValue = inst.getOperand(0);
				Value *annotated_variable = cast<Instruction>(annotatedValue)->getOperand(0);
				errs() << "annotatedValue ==" << *annotatedValue << '\n';
				errs() << "name is ==" << annotated_variable->getName().str() << '\n';

				//get annotation
				//https://stackoverflow.com/questions/4976298/modern-equivalent-of-llvm-annotationmanager
				Value *annotation = inst.getOperand(1);
				ConstantExpr *ce = (ConstantExpr *)annotation;
				// `ConstantExpr` operands: http://llvm.org/docs/LangRef.html#constantexprs
				Value *gv = ce->getOperand(0);
				errs() << "gv->getName() ==" << gv->getName().str() << '\n';
				errs() << "result ==" << getAnnotationString(gv->getName().str()) << '\n';
			}
		}
	}
}


int getParamlen(Function &fn, std::string argName) {
	for (auto &basic_block : fn) {
		for (auto &inst : basic_block) {
			Instruction *I = &inst;

			int num_operands = inst.getNumOperands();

			if (num_operands == 0) continue;

			Value *annotation_lable = inst.getOperand(inst.getNumOperands() - 1);
			if (annotation_lable->getName().str() != "llvm.var.annotation")continue;
			errs() << inst << '\n';

			// get annotated obj
			Value *annotatedValue = inst.getOperand(0);
			Value *annotated_variable = cast<Instruction>(annotatedValue)->getOperand(0);
			errs() << "annotatedValue ==" << *annotatedValue << '\n';
			errs() << "name is ==" << annotated_variable->getName().str() << '\n';

			std::string argNameWithSuffix = annotated_variable->getName().str();
			std::string argNameWithNoSuffix = argNameWithSuffix.substr(0, argNameWithSuffix.find(".addr"));

			errs() << "argName == " << argName << "\n";

			errs() << "argNameWithNoSuffix == " << argNameWithNoSuffix << "\n";

			if (argNameWithNoSuffix.compare(argName) == 0) {
				//get annotation
				//https://stackoverflow.com/questions/4976298/modern-equivalent-of-llvm-annotationmanager
				Value *annotation = inst.getOperand(1);
				ConstantExpr *ce = (ConstantExpr *) annotation;
				//ConstantExpr` operands: http://llvm.org/docs/LangRef.html#constantexprs
				Value *gv = ce->getOperand(0);

				errs() << "gv->getName() ==" << gv->getName().str() << '\n';

				errs() << "result ==" << getAnnotationString(gv->getName().str()) << '\n';

				return getAnnotationInt(gv->getName().str());
				//errs()<<"int ==" <<getAnnotationInt(m, gv->getName().str())<< '\n';

			}

		}
	}

	return 0;
}


std::set<Function*> getAnnotatedFn(llvm::Module &m) {
	module = &m;
	std::set<Function*> secure_fns;
	auto global_annos = m.getNamedGlobal("llvm.global.annotations");
	if (global_annos) {
		auto a = cast<ConstantArray>(global_annos->getOperand(0));
		for (int i = 0; i < a->getNumOperands(); i++) {
			auto e = cast<ConstantStruct>(a->getOperand(i));
			if (auto fn = dyn_cast<Function>(e->getOperand(0)->getOperand(0))) {
				auto anno = cast<ConstantDataArray>(cast<GlobalVariable>(e->getOperand(1)->getOperand(0))->getOperand(0))->getAsCString();

				if (anno == "server") {
					errs() << "server :\n";
					errs().write_escaped(fn->getName()) << '\n';
					secure_fns.insert(fn);
				}
			}
		}
	}

	return secure_fns;
}

void annoAnalysis(llvm::Module &m, std::set<struct FunctionInfo> &staFnInfoSet) {
	module = &m;
	std::set<Function*> secure_fns; // if the m.getFunction doesn't work, we can use these way to get the functions annoated with "partition"
	auto global_annos = m.getNamedGlobal("llvm.global.annotations");
	if (global_annos) {
		auto a = cast<ConstantArray>(global_annos->getOperand(0));
		for (int i = 0; i < a->getNumOperands(); i++) {
			auto e = cast<ConstantStruct>(a->getOperand(i));
			if (auto fn = dyn_cast<Function>(e->getOperand(0)->getOperand(0))) {
				struct FunctionInfo fnInfo;
				fnInfo.Name = fn->getName();
				struct FunctionInfo deleteInfo;
				if (!staFnInfoSet.empty()) {
					for (auto info : staFnInfoSet) {
						if (info.Name == fn->getName()) {
							fnInfo = info;
							deleteInfo = info;
							// if the name is unique, then, we can just break
							break;
							//staFnInfoSet.erase(info);
						}
					}
				}
              
				auto anno = cast<ConstantDataArray>(cast<GlobalVariable>(e->getOperand(1)->getOperand(0))->getOperand(0))->getAsCString();
				errs() << "anno :" << anno.str() << "\n";
				//using the built in annotatio for partitiong

				if (anno == "server") {
					errs() << "server............................................................>>>>>>>>>>>>>>>>>>>>>>>>>:\n";
					errs().write_escaped(fn->getName()) << '\n';
					secure_fns.insert(fn);
					fnInfo.isPatition = true;
					staFnInfoSet.erase(deleteInfo);
					staFnInfoSet.insert(fnInfo);
				}
				// vector<string> temp_anno;
				// if (!anno.empty())
				// 	split(anno.str(), ';', temp_anno);
				// //in case if use none built in annotation
				// if (!temp_anno.empty()) {
				// 	if (temp_anno.at(0) == "server") {
				// 		errs() << "Partition :\n";
				// 		errs() << fn->getName() << '\n';
				// 		secure_fns.insert(fn);
				// 		fnInfo.isPatition = true;
				// 	} else if (temp_anno.at(0) == "ExecutionTime") {
				// 		errs() << "ExecutionTime :\n";
				// 		errs() << fn->getName() << '\n';
				// 		fnInfo.exetime.RT_Type = temp_anno.at(1);
				// 		fnInfo.exetime.deadline = temp_anno.at(2);
				// 		fnInfo.exetime.ProfileMethod = temp_anno.at(3);
				// 	} else if (temp_anno.at(0) == "Frequency") {
				// 		errs() << "Frequency :\n";
				// 		errs() << fn->getName() << '\n';
				// 		fnInfo.frequency.RT_Type = temp_anno.at(1);
				// 		fnInfo.frequency.RT_Period = temp_anno.at(2);
				// 		fnInfo.frequency.RT_Times = temp_anno.at(3);
				// 	} else if (temp_anno.at(0) == "RTLoop") {
				// 		errs() << "RTLoop :\n";
				// 		errs() << fn->getName() << '\n';
				// 		fnInfo.rtLoop.LP_Counts = temp_anno.at(1);
				// 		fnInfo.rtLoop.LP_Iterations = temp_anno.at(2);
				// 	} else if (temp_anno.at(0) == "RTMemory") {
				// 		errs() << "RTMemory :\n";
				// 		errs() << fn->getName() << '\n';
				// 		fnInfo.rtMem.RT_Type = temp_anno.at(1);
				// 		fnInfo.rtMem.RT_MemSize = temp_anno.at(2);
				// 	} else if (temp_anno.at(0) == "RPC") {
				// 		errs() << "RPC :\n";
				// 		errs() << fn->getName() << '\n';
				// 		fnInfo.rpc.RT_Type = temp_anno.at(1);
				// 		fnInfo.rpc.Encryption = temp_anno.at(2);
				// 		fnInfo.rpc.Scheduler = temp_anno.at(3);
				// 	} else if (temp_anno.at(0) == "ExceptionHandler") {
				// 		errs() << "ExceptionHandler :\n";
				// 		errs() << fn->getName() << '\n';
				// 		fnInfo.ectHandler.RT_Type = temp_anno.at(1);
				// 		fnInfo.ectHandler.ExcTimes = temp_anno.at(2);
				// 		fnInfo.ectHandler.ExcHandler = temp_anno.at(3);
				// 	}

				// }
			}
		}
	}

}


std::set<GlobalVariable*> getAnnotatedGlobalVar(llvm::Module &m) {
	module = &m;

	// for global annotated_variable
	std::set<GlobalVariable*> secure_gVars;
	auto global_annos = m.getNamedGlobal("llvm.global.annotations");
	if (global_annos) {
		auto a = cast<ConstantArray>(global_annos->getOperand(0));
		for (int i = 0; i < a->getNumOperands(); i++) {
			auto e = cast<ConstantStruct>(a->getOperand(i));
			if (auto var = dyn_cast<llvm::GlobalVariable>(e->getOperand(0)->getOperand(0))) {
				auto anno = cast<ConstantDataArray>(cast<GlobalVariable>(e->getOperand(1)->getOperand(0))->getOperand(0))->getAsCString();

				if (anno == "trusted") {
					errs() << "trusted :\n";
					errs().write_escaped(var->getName()) << '\n';
					secure_gVars.insert(var);
				}

			}
		}
	}

	return secure_gVars;
}

std::set<Value *> getAnnotatedLocalVar(llvm::Module &m) {

	std::set<Value *> vars;
	for (auto &funct : m) {
		for (auto &basic_block : funct) {
			for (auto &inst : basic_block) {
				Instruction *I = &inst;
				// errs() << "i = " << i++ << '\n';
				// errs() << inst << '\n';moudle
				int num_operands = inst.getNumOperands();
				//errs() <<  << '\n';
				//errs() << I->getNumOperands() << '\n';
				if (num_operands == 0)continue;

				Value *annotation_lable = inst.getOperand(inst.getNumOperands() - 1);
				if (annotation_lable->getName().str() != "llvm.var.annotation")continue;
				errs() << inst << '\n';

				// get annotated obj
				Value *annotatedValue = inst.getOperand(0);
				Value *annotated_variable = cast<Instruction>(annotatedValue)->getOperand(0);
				errs() << "annotatedValue ==" << *annotatedValue << '\n';
				errs() << "name is ==" << annotated_variable->getName().str() << '\n';
				vars.insert(annotated_variable);

				//get annotation
				//https://stackoverflow.com/questions/4976298/modern-equivalent-of-llvm-annotationmanager
				Value *annotation = inst.getOperand(1);
				ConstantExpr *ce = (ConstantExpr *)annotation;
				// `ConstantExpr` operands: http://llvm.org/docs/LangRef.html#constantexprs
				Value *gv = ce->getOperand(0);
				errs() << "gv->getName() ==" << gv->getName().str() << '\n';
				errs() << "result ==" << getAnnotationString(gv->getName().str()) << '\n';
			}
		}
	}
	return vars;
}

/*
std::string getGlobalVariableString(std::string name)std::string getAnnotationString(std::string name)
{
    // assumption: the zeroth operand of a Value::GlobalVariableVal is the actual Value
    Value *v = module->getNamedValue(name)->getOperand(0);
    ConstantDataArray *ca = (ConstantDataArray *)v;
   // errs() << "result nb =======" << ca->getAsCString() <<'\n';
    return ca->getAsCString();
}
{
    // assumption: the zeroth operand of a Value::GlobalVariableVal is the actual Value
    Value *v = module->getNamedValue(name)->getOperand(0);
    ConstantDataArray *ca = (ConstantDataArray *)v;
    //errs() << "result =======" << ca->getName().str() <<'\n';
    //errs() << "result2 =======" << cast<ConstantDataArray>(ca)->getAsCString() <<'\n';
    errs() << "result nb =======" << ca->getAsCString() <<'\n';

    if(v->getValueID() == Value::ConstantArrayVal)
    {
        ConstantArray *ca = (ConstantArray *)v;
        return ca->getName();
	//->getAsString();
    }

    return "";llvm::Module &m
}*/
//
// void dumpFunctionArgAnnotations(Function &F)
// {
//     std::map<Value *,Argument*> mapValueToArgument;
//
//     Function *func = &F;
//     if(!func)
//     {
//         std::cout << "no function by that name.\n";
//         return;
//     }
//
//     errs().write_escaped(func->getName() ) << '\n';
//
//
//     // assumption: @llvm.var.annotation calls are always in the function's entry block.
//     BasicBlock *b = &func->getEntryBlock();
//
//
//     // run through entry block first to build map of pointers to arguments
//     for (Instruction &I : *b){
//        Instruction *inst = &I;
//         if(inst->getOpcode()!=Instruction::Store)
//             continue;
//
//         // `store` operands: http://llvm.org/docs/LangRef.html#i_store
//         mapValueToArgument[inst->getOperand(1)] = (Argument *)inst->getOperand(0);
//     }
//
//     // run through entry block a second time, to associate annotations with arguments
//     for (Instruction &I : *b){
//     {
//         Instruction *inst = &I; m
//         if(inst->getOpcode()!=Instruction::Call)
//             continue;
//
//         // assumption: Instruction::Call's operands are the function arguments, followed by the function name
//         Value *calledFunction = inst->getOperand(inst->getNumOperands()-1);
//         if(calledFunction->getName().str() != "llvm.var.annotation")
//             continue;
//
//         // `llvm.var.annotation` operands: http://llvm.org/docs/LangRef.html#int_var_annotation
//
//         Value *annotatedValue = inst->getOperand(0);
//         if(annotatedValue->getValueID() != Value::InstructionVal + Instruction::BitCast)
//             continue;
//         //Argument *a = mapValueToArgument[annotatedValue->getUnderlyingObject];
// 	//Argument *a = mapValueToArgument[annotatedValue->getNumUses()];
//         //if(!a)
//           //  continue;
//
//         Value *annotation = inst->getOperand(1);
//         if(annotation->getValueID() != Value::ConstantExprVal)
//             continue;
//         ConstantExpr *ce = (ConstantExpr *)annotation;
//         if(ce->getOpcode() != Instruction::GetElementPtr)
//             continue;
//
//         // `ConstantExpr` operands: http://llvm.org/docs/LangRef.html#constantexprs
//         Value *gv = ce->getOperand(0);
//
//         if(gv->getValueID() != Value::GlobalVariableVal)
//             continue;
//
//
// 	std::cout << " has annotation \"" << getGlobalVariableString(gv->getName().str()) << "\"\n";
//
// //         std::cout << "    argument " << a->getType()->getDescription() << " " << a->getName().str()
// //             << " has annotation \"" << getGlobalVariableString(gv->getName().str()) << "\"\n";
//     }
//   }
// }


#endif
