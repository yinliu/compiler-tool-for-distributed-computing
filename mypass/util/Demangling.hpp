#ifndef Demangling_HPP__
#define Demangling_HPP__

#include <cstdlib>
#include <cxxabi.h>
#include <iostream>

std::string demanglingName(std::string fnName){
    char * c_demangled;
    char const * mangled = fnName.c_str();
    int status = 0;
    c_demangled = abi::__cxa_demangle(mangled, nullptr, nullptr, &status);
    // std::cout<<"demangled name =========" << c_demangled << std::endl;
    std::string ret (c_demangled);
    std::size_t pos = ret.find_first_of("(");

    return ret.substr(0,pos);
}


std::string demanglingStructTypeName(std::string structName){
  //%struct.differential_pressure_s*
  std::string::size_type percent_pos = structName.find("%");
  if (percent_pos != std::string::npos)
    structName.erase(percent_pos, 1);
  
  std::string::size_type point_pos = structName.find(".");
  if (point_pos != std::string::npos)
    structName.replace(point_pos, 1, " ");
  
  return structName;
}

std::string demanglingStructVarName(std::string structName){
  //%struct.differential_pressure_s*
  std::string::size_type dot_pos = structName.find(".");
  std::string::size_type end_pos = structName.find_last_of(structName);
  
  if (dot_pos != std::string::npos)
    structName.erase(dot_pos, end_pos);

  return structName;
}


#endif