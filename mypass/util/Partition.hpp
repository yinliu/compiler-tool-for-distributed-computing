#ifndef Partition_HPP__
#define Partition_HPP__


#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include <llvm/IR/Constants.h>
#include "llvm/Support/raw_ostream.h"
#include <../../../LLVM/llvm/include/llvm/IR/BasicBlock.h>
#include <set>
#include <iostream>
#include <fstream>
//#include "./Demangling.hpp" // need to consider the header file's order, need to use #ifndef to slove it later


using namespace llvm;
using namespace std;

void partitionFn(llvm::Module &m, llvm::Function &f, string FileName ){
    std::ofstream sf;
    //std::ostringstream ostr;
  
     std::string funcName = f.getName();
    // we need to check, if it is .c file, then, don't need to do demangling
    //std::string funcName = demanglingName(f.getName().str());
    errs() << "\n partitionFn is ============= :\n";
    errs().write_escaped(funcName) << '\n';
    errs() << f <<'\n';

    std::string str_partitionedFn;
    llvm::raw_string_ostream ros_partitionedFn(str_partitionedFn);
    f.print(ros_partitionedFn);
    
    // create a new module and cloneFunction the function and partition to another file and see whether it could be linked
	    
	    
    sf.open(FileName +".ll", ios::out | ios::app);
    if (sf.is_open()) {
	sf << ros_partitionedFn.str() << endl;
	sf.close();
    }
	    
}

#endif