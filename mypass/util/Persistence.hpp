#ifndef Persistence_HPP__
#define Persistence_HPP__

#include <fstream>
#include <set>
#include <sstream>
#include <vector>

#include "common.h"
#include "StrOperation.hpp"

using namespace std;
using namespace llvm;

#define PersistenceADDR "Persistence"

class Persistence {
private:
  ofstream sf;
  ostringstream ostr;

  void prepare();
  void release();

public:
  Persistence(){}
  void persist(std::set<struct FunctionInfo> fnInfoSet);
  std::set<struct FunctionInfo> readback();
};

void Persistence::prepare()
{
  sf.open(PersistenceADDR, ios::out | ios::app);
}

void Persistence::release()
{
  if(sf && sf.is_open())
    sf.close();
  
  if(ostr)
    ostr.str("");
}

void Persistence::persist(std::set<struct FunctionInfo> fnInfoSet)
{
  prepare();
  int num = 1;
  for(struct FunctionInfo fnInfo : fnInfoSet){
    // id
    if (fnInfo.ID == -1) continue;
    ostr << fnInfo.ID << ";";

    cout  << "writing a file.......................................................................................ooooooooooooooooooooooooo";

    // name
    if (fnInfo.Name.empty()) continue;
    ostr << fnInfo.Name << ";";
    
    // bool isMangling
    ostr << fnInfo.isMangling << ";";
    
    // bool hasStruct
    ostr << fnInfo.hasStruct << ";";

    //std::string ReturnType
    if (fnInfo.ReturnType.empty())
      ostr << "null" << ";";
    else
      ostr << fnInfo.ReturnType << ";";

    // int argNum = 0;
    ostr << fnInfo.argNum << ";";

    // paramDecls
    if (fnInfo.paramDecls.empty())
      ostr << "null" << ";";
    else {
      ostr << fnInfo.paramDecls.size() << ";";
      for(struct ParamDecl paramD : fnInfo.paramDecls){
	ostr << paramD.fnName << ";";
	ostr << paramD.Name << ";";
	ostr << paramD.Type << ";";

	if (paramD.structInfo.name.empty())
	  ostr << "null" << ";";
	else {
	  ostr << paramD.structInfo.name << ";";
	  ostr << paramD.structInfo.items.size() << ";";
	  for(pair<string,string> pair : paramD.structInfo.items){
	    ostr << pair.first << ";";
	    ostr << pair.second << ";";
	  }
	}
      }
    }
    
    // bool isZigZag
    ostr << fnInfo.isZigZag << ";";
    
    // bool hasGlobalVars
    ostr << fnInfo.hasGlobalVars << ";";
    
    // bool canBePartition
    ostr << fnInfo.canBePartition << ";";
    
    // globalVarDecls
    if (fnInfo.globalVarDecls.empty())
      ostr << "null" << ";";
    else {
      ostr << fnInfo.globalVarDecls.size() << ";";
      for (struct GlobalVarDecl gv : fnInfo.globalVarDecls){
	ostr << gv.Name << ";";
	ostr << gv.Type << ";";
	ostr << gv.userFnName.size() << ";";
	for (auto fnName : gv.userFnName){
	  ostr << fnName << ";";
	}
	
      }
    }
    
    //bool isPatition
    ostr << fnInfo.isPatition << ";";
    
    //struct ExecutionTime exetime
    if(fnInfo.exetime.RT_Type != ""
          && fnInfo.exetime.deadline != ""
          && fnInfo.exetime.ProfileMethod != "") {
      ostr << fnInfo.exetime.RT_Type << ";";
      ostr << fnInfo.exetime.deadline << ";";
      ostr << fnInfo.exetime.ProfileMethod << ";";
    } else {
      ostr << "null" << ";";
      ostr << "null" << ";";
      ostr << "null" << ";";
    }
    
    //struct Frequency frequency
    if(fnInfo.frequency.RT_Type != ""
          && fnInfo.frequency.RT_Times != ""
          && fnInfo.frequency.RT_Period != "") {
      ostr << fnInfo.frequency.RT_Type << ";";
      ostr << fnInfo.frequency.RT_Times << ";";
      ostr << fnInfo.frequency.RT_Period << ";";
    } else {
      ostr << "null" << ";";
      ostr << "null" << ";";
      ostr << "null" << ";";
    }
    
    
    //struct RTLoop rtLoop
    if(fnInfo.rtLoop.LP_Counts != ""
          && fnInfo.rtLoop.LP_Iterations != "") {
      ostr << fnInfo.rtLoop.LP_Counts << ";";
      ostr << fnInfo.rtLoop.LP_Iterations << ";";
    } else {
      ostr << "null" << ";";
      ostr << "null" << ";";
    }
    
    //struct RTMemory rtMem
    if(fnInfo.rtMem.RT_Type != ""
          && fnInfo.rtMem.RT_MemSize != "") {
      ostr << fnInfo.rtMem.RT_Type << ";";
      ostr << fnInfo.rtMem.RT_MemSize << ";";
    } else {
      ostr << "null" << ";";
      ostr << "null" << ";";
    }
    
    //struct RPC rpc 
    if(fnInfo.rpc.RT_Type != ""
          && fnInfo.rpc.Encryption != ""
          && fnInfo.rpc.Scheduler != "") {
      ostr << fnInfo.rpc.RT_Type << ";";
      ostr << fnInfo.rpc.Encryption << ";";
      ostr << fnInfo.rpc.Scheduler << ";";
    } else {
      ostr << "null" << ";";
      ostr << "null" << ";";
      ostr << "null" << ";";
    }
    
    //struct ExceptionHandler ectHandler
    if(fnInfo.ectHandler.RT_Type != ""
          && fnInfo.ectHandler.ExcTimes != ""
          && fnInfo.ectHandler.ExcHandler != "") {
      ostr << fnInfo.ectHandler.RT_Type << ";";
      ostr << fnInfo.ectHandler.ExcTimes << ";";
      ostr << fnInfo.ectHandler.ExcHandler << ";";
    } else {
      ostr << "null" << ";";
      ostr << "null" << ";";
      ostr << "null" << ";";
    }

    ostr << "\n";
  }

  sf << ostr.str() << endl;
  release();
}

std::set<struct FunctionInfo> Persistence::readback()
{
    std::set<struct FunctionInfo> ret;
    ifstream fin(PersistenceADDR);  
    string s;  
    while( getline(fin,s) )
    {
      cout << "Read from file: " << s << endl;
      
      struct FunctionInfo fnInfo = {};
      vector<string> temp_vec;
      split(s, ';',temp_vec);
      
      if(temp_vec.empty()) continue;
      
      int i_vec = 0;
      int temp_i = 0;
      
      // 0. id
      cout << "fnInfo.ID: " << temp_vec.at(0) << endl;
      fnInfo.ID = stoi(temp_vec.at(0), nullptr);
      
      // 1. name
      cout << "fnInfo.Name: " << temp_vec.at(1) << endl;
      fnInfo.Name = temp_vec.at(1);
      
      // 2. bool isMangling
      cout << "fnInfo.isMangling: " << temp_vec.at(2) << endl;
      fnInfo.isMangling = (temp_vec.at(2) == "1") ? true : false;
      
      // 3. bool isMangling
      cout << "fnInfo.hasStruct: " << temp_vec.at(3) << endl;
      fnInfo.hasStruct = (temp_vec.at(3) == "1") ? true : false;
      
      // 4. std::string ReturnType
      cout << "fnInfo.ReturnType: " << temp_vec.at(4) << endl;
      fnInfo.ReturnType = (temp_vec.at(4) != "null") ? temp_vec.at(4) : "";
      
      // 5. int argNum = 0;
      cout << "fnInfo.argNum: " << temp_vec.at(5) << endl;
      fnInfo.argNum = stoi(temp_vec.at(5), nullptr);
      
      // 6. paramDecls
      i_vec = 6;
      if (temp_vec.at(6) != "null") {
	cout << " stoi(temp_vec.at(6), nullptr); " << temp_vec.at(6) << endl;
	int num_param = stoi(temp_vec.at(6), nullptr);
	for(int i = 0; i < num_param; i++){
	  struct ParamDecl paramD = {};
	  paramD.fnName = temp_vec.at(++i_vec);
	  paramD.Name = temp_vec.at(++i_vec);
	  paramD.Type = temp_vec.at(++i_vec);
	  
	  // structInfo
	  temp_i = i_vec + 1;
	  if (temp_vec.at(temp_i) == "null"){
	    paramD.structInfo = {};
	    ++i_vec; 
	  }
	  else {
	    paramD.structInfo.name = temp_vec.at(++i_vec);
	    int num_struct = stoi(temp_vec.at(++i_vec) ,nullptr);
            int i_pair = 0;
	    for(int i = 0; i < num_struct; i++) {
	       pair<string,string> item_struct;
	       item_struct.first = temp_vec.at(++i_vec);
	       item_struct.second = temp_vec.at(++i_vec);
	       paramD.structInfo.items.push_back(item_struct);
	    } 
	  }
	  fnInfo.paramDecls.push_back(paramD);
	}
      }
      
      // 6.  bool isZigZag
      fnInfo.isZigZag =  (temp_vec.at(++i_vec) == "1") ? true : false;
      
      // 7.  bool hasGlobalVars
      fnInfo.hasGlobalVars =  (temp_vec.at(++i_vec) == "1") ? true : false;
      
      // 8.  bool canBePartition
      fnInfo.canBePartition =  (temp_vec.at(++i_vec) == "1") ? true : false;
      
      // 9. globalVarDecls
      temp_i = i_vec + 1;
      if (temp_vec.at(temp_i) == "null") {
	fnInfo.globalVarDecls = {};
	++i_vec; 
      } else {
	cout << " num_gv " << temp_vec.at(i_vec+1) << endl;
	int num_gv = stoi(temp_vec.at(++i_vec) ,nullptr);
	for(int i = 0; i < num_gv; i++){
	  struct GlobalVarDecl gv = {};
	  gv.Name = temp_vec.at(++i_vec);
	  gv.Type = temp_vec.at(++i_vec);
	  cout << " num_fn " << temp_vec.at(i_vec+1) << endl;
	  int num_fn = stoi(temp_vec.at(++i_vec) ,nullptr);
	  for(int j = 0; j < num_fn; j++){
	    gv.userFnName.insert(temp_vec.at(++i_vec));
	  }
	  fnInfo.globalVarDecls.insert(gv);
	}
      }
      
      // 10. bool isPatition
      fnInfo.isPatition =  (temp_vec.at(++i_vec) == "1") ? true : false;
      
      // 11. struct ExecutionTime exetime
      fnInfo.exetime.RT_Type = temp_vec.at(++i_vec);
      fnInfo.exetime.deadline = temp_vec.at(++i_vec);
      fnInfo.exetime.ProfileMethod = temp_vec.at(++i_vec);
      
      // 12. struct Frequency frequency
      fnInfo.frequency.RT_Type = temp_vec.at(++i_vec);
      fnInfo.frequency.RT_Times = temp_vec.at(++i_vec);
      fnInfo.frequency.RT_Period = temp_vec.at(++i_vec);
      
      //13. struct RTLoop rtLoop
      fnInfo.rtLoop.LP_Counts = temp_vec.at(++i_vec);
      fnInfo.rtLoop.LP_Iterations = temp_vec.at(++i_vec);
      
      //14. struct RTMemory rtMem
      fnInfo.rtMem.RT_Type = temp_vec.at(++i_vec);
      fnInfo.rtMem.RT_MemSize = temp_vec.at(++i_vec);
      
      //15. struct RPC rpc
      fnInfo.rpc.RT_Type = temp_vec.at(++i_vec);
      fnInfo.rpc.Encryption = temp_vec.at(++i_vec);
      fnInfo.rpc.Scheduler = temp_vec.at(++i_vec);
      
      //16. struct ExceptionHandler ectHandler
      fnInfo.ectHandler.RT_Type = temp_vec.at(++i_vec);
      fnInfo.ectHandler.ExcTimes = temp_vec.at(++i_vec);
      fnInfo.ectHandler.ExcHandler = temp_vec.at(++i_vec);

      ret.insert(fnInfo);
    }
    return ret;
}

#endif
