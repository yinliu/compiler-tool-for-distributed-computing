#ifndef Refactoring_HPP__
#define Refactoring_HPP__

#include <stdio.h>
#include <iostream>
#include <string>
#include <sstream>

void InsertLine(char* FileName, int Line, char str[256]) {
	int Lid = 0;
	int MaxLine = 0;
	FILE* fp = NULL;
	char Buf[1024] = "";
	char tmp[500][1024] = { 0 };

	if ((fp = fopen(FileName, "r+")) == NULL) {
		printf("Can't   open   file!/n");
		return;
	}
	while (fgets(Buf, 1024, fp)) {
		Lid++;
		if (Lid == Line) {
			strcpy(tmp[Lid++], str);
		}
		strcpy(tmp[Lid], Buf);
	}

	MaxLine = Lid;
	rewind(fp);
	for (Lid = 1; Lid <= MaxLine; Lid++) {
		fputs(tmp[Lid], fp);
	}

	fclose(fp);
}

#endif