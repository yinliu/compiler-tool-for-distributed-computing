#ifndef StrOP_HPP__
#define StrOP_HPP__

#include <vector>
#include <string>

using namespace std;

void split(const string& s, char delim, vector<string>& v) {
    auto i = 0;
    auto pos = s.find(delim);
    while (pos != string::npos) {
      v.push_back(s.substr(i, pos-i));
      i = ++pos;
      pos = s.find(delim, pos);

      if (pos == string::npos)
         v.push_back(s.substr(i, s.length()));
    }
}


#endif