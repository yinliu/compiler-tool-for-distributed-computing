#ifndef Struct_HPP__
#define Struct_HPP__

#include "llvm/IR/Function.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/Constants.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/DebugInfo.h"
#include "llvm/ADT/DenseMap.h"
#include "llvm/ADT/DenseSet.h"
#include "llvm/ADT/None.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/ADT/StringRef.h"
#include "llvm/IR/DebugInfoMetadata.h"
#include "llvm/IR/DebugLoc.h"
#include "llvm/IR/Function.h"
#include "llvm/IR/GVMaterializer.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/IntrinsicInst.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Metadata.h"
#include "llvm/Support/Casting.h"

#include <vector>
#include <iostream>
#include "common.h"
using namespace llvm;
using namespace std;

struct StructInfo  getStructType(Module &M, string funcName){
errs() << "getStructType ++++++++++++++++++++++" <<"\n";
DebugInfoFinder Finder;
Finder.processModule(M); //get all the metadata first

struct StructInfo ret;

for (auto &s : Finder.subprograms()) {
  
  // just check the annotated function
  if (funcName != s->getName()) continue;
  
    DITypeRefArray  typeArr =  s->getType()->getTypeArray();
    for (auto type : typeArr){
      llvm::DIType* myDIType = type.resolve();
      if (NULL == myDIType
	|| myDIType->getTag() != dwarf::DW_TAG_structure_type
      ) continue;

      llvm::DICompositeType* compositeType = (llvm::DICompositeType*)myDIType;
      if (NULL == compositeType) continue;
      ret.name = compositeType->getName();

      DINodeArray nodeArr = compositeType->getElements();
      
      struct StructItem item;
      
      for(auto node : nodeArr){
	llvm::DIDerivedType* derivedType = (llvm::DIDerivedType*)node;
	//ret.itemsName.push_back(derivedType->getName());

	string itemsName = derivedType->getName();
	int itemSize;
	string itemsType;
	if (DIBasicType::classof(derivedType->getBaseType())){
	  // for basic type
	  itemsType = derivedType->getBaseType().resolve()->getName();
	  itemSize = derivedType->getSizeInBits();
	  //ret.itemsType.push_back(derivedType->getBaseType().resolve()->getName());
	  //errs() << "type name == " <<  derivedType->getBaseType().resolve()->getName() <<"\n";
	} else if (DIDerivedType::classof(derivedType->getBaseType())){
	    // for derived type
	    //errs() << "type name == " <<  derivedType->getBaseType().resolve()->getName() <<"\n";
	    //zyx->getBaseType().dump();
	    llvm::DIDerivedType* subDerivedType = (DIDerivedType*)(derivedType->getBaseType().resolve());
	    itemSize = subDerivedType->getSizeInBits();
	    if (subDerivedType->getTag() == dwarf::DW_TAG_pointer_type){
	      itemsType = subDerivedType->getBaseType().resolve()->getName();
	      itemsType.append("*");
	      //string typeName = subDerivedType->getBaseType().resolve()->getName();
	      //typeName.append("*");
	      //ret.itemsType.push_back(typeName);
	    }
	    //subDerivedType->dump();
	    //errs() << "type name == " <<  subDerivedType->getBaseType().resolve()->getName() <<"\n";
	} else if (DICompositeType::classof(derivedType->getBaseType())){
	  // for Composite Type
	  llvm::DICompositeType* subCompositeType = (DICompositeType*)(derivedType->getBaseType().resolve());
	  errs() << "DICompositeType type size == " <<  subCompositeType->getSizeInBits() <<"\n";
	  itemSize = subCompositeType->getSizeInBits();
	  unsigned tag = subCompositeType->getTag();
	  switch (tag){
	    case dwarf::DW_TAG_array_type:
	    {
	      vector<int> arrCounts;
	      itemsType = subCompositeType->getBaseType().resolve()->getName();
	      //FIXME if subCompositeType->getBaseType() is DIDerivedType
	      DINodeArray array_nodes = subCompositeType->getElements();
	      for(auto arr_node : array_nodes){
		 llvm::DISubrange* range = (llvm::DISubrange*)arr_node;
		 int arr_count = range->getCount();
		 arrCounts.push_back(arr_count);
		 errs() << "range->getCount() == " <<  range->getCount() <<"\n";
		 errs() << "itemsType == " <<  itemsType <<"\n";
	      }
	      item.arrCounts = arrCounts;
	      break;
	    }
	    case dwarf::DW_TAG_typedef:
	    {
	      break;
	    }
	    case dwarf::DW_TAG_structure_type:{
	      break;
	    }
	  }
	  
	  
	  
	}
	
	pair<string, string> itmesPair = {itemsName, itemsType};
	item.name = itemsName;
	item.type = itemsType;
	item.size = itemSize;
	ret.struct_items.push_back(item);
	ret.items.push_back(itmesPair);
      }
    }
  
  errs() << "struct name == " <<  ret.name <<"\n";
  
  for(auto ipair : ret.items){
    errs() << "var name == " <<  ipair.first <<"\n";
    errs() << "type name == " <<  ipair.second <<"\n";
  }
  
  return ret;
  
//   for(auto iName : ret.itemsName){
//     errs() << "struct var name == " <<  iName <<"\n";
//   }
//   for(auto iType : ret.itemsType){
//     errs() << "struct var type == " <<  iType <<"\n";
//   }
}
  
}





/*
 * Some code for reference
 */

/**
 * traverse all the types of metadata
 */
/*
for(auto &t : Finder.types()){
  //t llvm::DIType
  t->dump();

  errs() <<  t->getRawName()<<"\n";
  errs() <<  t->getName() <<"\n";
}*/


/**
 * code from the DebugInfo.cpp
 */
    /*
    if (auto *SP = cast_or_null<DISubprogram>(F.getSubprogram()))
      //processSubprogram(SP);
    // There could be subprograms from inlined functions referenced from
    // instructions only. Walk the function to find them.
    for (const BasicBlock &BB : F) {
      for (const Instruction &I : BB) {
        if (!I.getDebugLoc())
          continue;
        //processLocation(M, I.getDebugLoc().get());
      }
    }
  }*/


/*
 * previous code for check the LLVM APIs
 */
 /*
  s->dump();
  s->getType()->dump();// return  llvm::DISubroutineType
  s->getType()->getTypeArray()->dump();

  DITypeRefArray  typeArr =  s->getType()->getTypeArray();
  for (auto sb : typeArr){
    llvm::DIType* what = sb.resolve();
    errs() << "sb" <<  sb.resolve() <<"\n";
    if (NULL != sb.resolve()) {
      errs() << "sb == " <<  sb.resolve()->getName() <<"\n";
      errs() << "sb tag == " <<  sb.resolve()->getTag() <<"\n";
            
      if(what->getTag() == dwarf::DW_TAG_structure_type){
	errs() << "what???????????????????????????????????????????" <<"\n";
	errs() << "what == " <<  what->getName() <<"\n";
	llvm::DICompositeType* caonima = (llvm::DICompositeType*)what; //DICompositeType
	caonima->getElements()->dump();
	DINodeArray ouyou =  caonima->getElements();
	for(auto pp: ouyou){
	  pp->dump(); // pp is llvm::DINode
	  
	  //pp->getType()->dump();
	  
	  //errs() << "caonimazhongyuchenggongle " <<  ((llvm::DIType*)pp)->getName() <<"\n";
	  
	  
	  llvm::DIDerivedType* zyx = (llvm::DIDerivedType*)pp;
	  
	  //FIXME here only support struct only contains basic type pointer, not the pointer of pointer ...... or pointer of struct
	  //maybe we can add recurrsive function to analyze the struct, and finally support pointer of struct, such as a linked list implemented by struct
	  if (DIBasicType::classof(zyx->getBaseType())){
	    errs() << "zyx?????????" <<  zyx->getBaseType().resolve()->getName() <<"\n";
	  } else if (DIDerivedType::classof(zyx->getBaseType())){
	    
	    errs() << "zyx---here?????????" <<  zyx->getBaseType().resolve()->getName() <<"\n";
	    //zyx->getBaseType().dump();
	    llvm::DIDerivedType* zyx2 = (DIDerivedType*)(zyx->getBaseType().resolve());
	    errs() << "thatisit?????????" <<  zyx2->getBaseType().resolve()->getName() <<"\n";
	  }

	  errs() << "zyx????????? " <<  zyx->getName() <<"\n";
  
	}
	
	
      }
      
    }
    
  }
  
  errs() <<  s->getName() <<"\n";
  */










#endif