#ifndef TCGen_HPP__
#define TCGen_HPP__

#include <fstream>
#include <sstream>
#include <string>

#define TIMEADDR "timeaddr.c"
#define TIMEID 1
#define DO_RECORD 1
#define DO_SAMPLING 2
#define DO_FREQUENCY 3

using namespace std;

class TCGenerator {
public:
  TCGenerator(){}
  void exeTimeTestGen();
  void frequencyTestGen();
private:
  std::ofstream sf;
  std::ostringstream ostr;
  void prepare(string addr);
  void release();
  void constructExeTime(ostringstream &ostr);
  void constructFrequency(ostringstream &ostr);
  void constructHeader(ostringstream &ostr, int id);
  void constructStart(ostringstream &ostr, int id);
  void constructStop(ostringstream &ostr, int id);
  bool isExeTimeGen = false;
  bool isFrequencyGen = false;
};

void TCGenerator::prepare(string addr)
{
  sf.open(addr, ios::out | ios::app);
}

void TCGenerator::release()
{
  if (sf.is_open()) sf.close();
  ostr.str("");
}

void TCGenerator::exeTimeTestGen()
{
  prepare(TIMEADDR);
  constructExeTime(ostr);
  sf << ostr.str() << endl;
  release();
  isExeTimeGen = true;
}

void TCGenerator::frequencyTestGen()
{
  prepare(TIMEADDR);
  constructFrequency(ostr);
  sf << ostr.str() << endl;
  release();
  isFrequencyGen = true;
}

void TCGenerator::constructExeTime(ostringstream& ostr)
{
  if (!isExeTimeGen) {
    constructHeader(ostr, TIMEID);
  }
  constructStart(ostr, DO_RECORD);
  constructStop(ostr, DO_RECORD);
}

void TCGenerator::constructFrequency(ostringstream& ostr)
{
  if (!isFrequencyGen) {
    constructHeader(ostr, TIMEID);
  }
  constructStart(ostr, DO_RECORD);
}

void TCGenerator::constructHeader(ostringstream& ostr, int id)
{
  ostr << "#include <stdio.h>" "\n";
  ostr << "#include <sys/time.h>" "\n";
}

void TCGenerator::constructStart(ostringstream& ostr, int id)
{
  ostr << "void ProfilerStart(char* str){" "\n";
  ostr << "struct timeval tmStart"  << ";" "\n";
  ostr << "gettimeofday(&tmStart, NULL)"  << ";" "\n";
  ostr << "}"  << ";" "\n";
}

void TCGenerator::constructStop(ostringstream& ostr, int id)
{
  ostr << "void ProfilerStop(char* str){" "\n";
  ostr << "timeval tmStop"  << ";" "\n";
  ostr << "gettimeofday(&tmStop, NULL)"  << ";" "\n";
  ostr << "}"  << ";" "\n";
}

#endif