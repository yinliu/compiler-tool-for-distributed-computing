#ifndef Common_HPP__
#define Common_HPP__
#include <vector>
#include <iostream>
using namespace llvm;
using namespace std;

struct StructItem{
  string name;
  string type;
  vector<int> arrCounts;
  int size;
};

struct StructInfo{
  string name;
  vector<pair<string,string>> items;
  vector<struct StructItem> struct_items;
//   vector<string> itemsName;
//   vector<string> itemsType;
  struct StructInfo* subStructInfo;// for test struct nest
};

struct GlobalVarDecl {
	int size = 0;
	std::set<string> userFnName;
	std::string Name;
	std::string Type;
	int location = 0;
	bool operator < (const GlobalVarDecl &other) const { return Name < other.Name; }
};


struct ParamDecl {
	int size = 0;
	std::string fnName;
	std::string Name;
	std::string Type;
	struct StructInfo structInfo;
	bool operator < (const ParamDecl &other) const { return Name < other.Name; }
};

struct ExecutionTime {
  string RT_Type;
  string deadline;
  string ProfileMethod;
};

struct Frequency {
  string RT_Type;
  string RT_Period;
  string RT_Times;
};

struct RTLoop {
  string LP_Counts;
  string LP_Iterations;
};

struct RTMemory {
  string RT_Type;
  string RT_MemSize;
};

struct RPC {
  string RT_Type;
  string Encryption;
  string Scheduler;
};

struct ExceptionHandler {
  string RT_Type;
  string ExcTimes;
  string ExcHandler;
};

struct FunctionInfo {
        int ID = -1;
	std::string Name;
	bool isMangling = false;
	std::string DemangledName;
	std::string ReturnType;
	int argNum = 0;
	std::vector<struct ParamDecl> paramDecls;
	bool hasStruct = false;

	// for zigzag and global var reports
	bool isZigZag = false;
	bool hasGlobalVars = false;
	bool canBePartition = true;
	std::set<struct GlobalVarDecl> globalVarDecls;
	int location = 0;
	int groupNo = -1;
	std::string reportInfo;
	////////////////////////////////////
	
	// for annotations
	// Partition
	bool isPatition = false;
	
	// ExecutionTime
	struct ExecutionTime exetime = {"","",""};
	
	// Frequency
	struct Frequency frequency = {"","",""};
	
	// RTLoop
	struct RTLoop rtLoop = {"",""};
	
	// RTMemory
	struct RTMemory rtMem = {"",""};
	
	// RPC
	struct RPC rpc = {"","",""};
	
	// ExceptionHandler
	struct ExceptionHandler ectHandler = {"","",""};
	
	//////////////////////////////////////////////////
	
	bool operator < (const FunctionInfo &other) const { return Name < other.Name; }
};





#endif